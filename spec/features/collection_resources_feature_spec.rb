# frozen_string_literal: true

require 'rails_helper'

feature 'CollectionResources' do
  context 'renders a resource page' do
    scenario 'including a title and content' do
      pending('Need better way to seed')
      visit collection_resource_path 'dlg_vsbg', 'bibliography'

      expect(page).to have_text 'Bibliography'
      expect(page).to have_text 'Bibliographic content'
    end
    scenario 'including collection name' do
      pending('Need better way to seed')
      visit collection_resource_path 'dlg_vsbg', 'bibliography'

      expect(page).to have_text 'Liberty Ships'
    end
  end
end