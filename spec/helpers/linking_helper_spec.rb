# frozen_string_literal: true

require 'rails_helper'

describe LinkingHelper do
  describe '#link_to_collection_page' do
    it 'on a record page for a collection, makes the title a link to the collection' do
      opt = {
        document: SolrDocument.new(
          id: 'test_collection',
          class_name_ss: 'Collection',
          title: 'Test Name'
        )
      }
      expect(link_to_collection_page(opt)).to(
        eq link_to 'Test Name', collection_home_path('test_collection')
      )
    end
  end
end
