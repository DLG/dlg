# frozen_string_literal: true

require 'rails_helper'

describe AdvancedHelper do
  describe '#select_menu_for_field_operator_with_aria_label' do
    it 'includes an aria-label attribute' do
      select_tag = select_menu_for_field_operator_with_aria_label
      expect(select_tag).to include 'aria-label="any/all"'
    end
  end
end
