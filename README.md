# Digital Library of Georgia

A Blacklight-powered frontend for the DLG's Georgia content.

Queries and displays data from the DLG's `meta` database and search index, which is managed by the [Meta administrative app](https://github.com/GIL-GALILEO/meta).

Project is also [on Github](https://github.com/GIL-GALILEO/dlg/)

### Requirements

- Ruby 3.2.2

### Development Setup

For Ubuntu 20.04

1. You may need some packages...
    1. `sudo apt-get update`
    2. `sudo apt-get install gcc build-essential libssl-dev libreadline-dev zlib1g-dev libpq-dev`
2. Install Ruby
    1. [Install rbenv](https://github.com/rbenv/rbenv-installer#rbenv-installer), ensure `rbenv` binaries are on your `$PATH`
    2. Install Ruby `3.2.2` using `rbenv install 3.2.2`
    3. Finish [setting up `rbenv`](https://github.com/rbenv/rbenv#installation)
3. Clone repo
4. Get or create a `master.key` file from someone special
5. Install application gems
   1. `gem install bundler`
   2. `bundle install`
6. Setup database tables using `rake db:setup`.  
7. Index sample data using `./reload_solr_data.sh`.  Note: Needed for tests/specs
8. `rails s` to start development server
9. Visit [localhost:3000](http://localhost:3000)

### License
© 2025  Digital Library of Georgia
