# frozen_string_literal: true

# abstract class for system emails
class HarmfulContentMailer < ApplicationMailer
  def harmful_content_submission(data)
    data[:submitted_at] = Time.now
    @harmful_content = OpenStruct.new(data)
    mail(
      to: Rails.application.credentials.harmful_content_recipients.dig(Rails.env.to_sym),
      subject: 'DLG Harmful Content Form Submission',
      template_path: 'mailers'
    )
  end
end
