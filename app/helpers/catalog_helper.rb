# frozen_string_literal: true

# helper methods for Blacklight catalog pages
module CatalogHelper
  include Blacklight::CatalogHelperBehavior
  COORDINATES_REGEXP = /(-?\d+\.\d+), (-?\d+\.\d+)/
  INDEX_TRUNCATION_VALUE = 2500

  def search_bar_placeholder
    if @collection
      I18n.t 'search.bar.placeholder.collection', collection: @collection.display_title
    elsif @institution
      I18n.t 'search.bar.placeholder.institution', institution: @institution.authorized_name
    elsif @serial
      I18n.t 'search.bar.placeholder.serial', serial: @serial.dcterms_title
    elsif controller.class.name.downcase =~ /collections/
      I18n.t 'search.bar.placeholder.collections'
    elsif controller.class.name.downcase =~ /serials/
      I18n.t 'search.bar.placeholder.serials'
    else
      I18n.t('search.bar.placeholder.default')
    end
  end

  def new_search_bar_placeholder
    if FeatureFlags.enabled? :new_search_boxes
      I18n.t('search.bar.placeholder.default')
    else
      search_bar_placeholder
    end
  end

  # @return [Hash]
  def search_fields_for_dropdown
    blacklight_config.search_fields.filter do | field_name, field_config |
      should_render_field? field_config
    end
  end

  # Returns the search type ("metadata", "fulltext", or "both") that's should be selected in the dropdown
  def search_type_key
    search_field_param = params['search_field']
    if search_field_param.present? && search_fields_for_dropdown.include?(search_field_param)
      return search_field_param
    end
    'both'
  end

  # find current search type in fields array and return display label
  def search_type_label
    field = search_fields_for_dropdown[search_type_key]
    I18n.t('blacklight.search.form.search.prefix') + ' ' + field.label
  end

  # Returns the list of Blacklight "search fields" for the search bar dropdown.
  # These are set up in the controller using:
  #   configure_blacklight do |config|
  #     ...
  #     config.add_search_field('key_name') do |field|
  #       ...
  #       field.include_in_simple_select = true
  #
  # You can provide an optional list of keys to determine the order of the fields, but it has no filtering effect.
  # Key names that aren't recognized will be ignored. Search fields whose keys are not in ordered_keys will be included
  #   at the end of the list after the explicitly ordered items.
  #
  # @param *ordered_keys [Array<String>]
  # @return [Array<Blacklight::FieldConfig>]
  def ordered_search_fields(*ordered_keys)
    search_fields = search_fields_for_dropdown
    ordered = []
    ordered_keys.each do |k|
      ordered << search_fields[k] if search_fields[k]
      search_fields.delete k
    end
    ordered + search_fields.values
  end

  # show search bar options only on homepage and record search
  def show_search_bar_options
    zone = controller.class.name.downcase
    zone =~ /homepage/ || zone =~ /records/
  end

  def strip_html(options = {})
    strip_tags(options[:value].first)
  end

  # truncate fields when displaying search results
  def truncate_index(options = {})
    truncate(
      options[:value].join(I18n.t('support.array.two_words_connector')),
      length: INDEX_TRUNCATION_VALUE,
      omission: I18n.t('search.index.truncated_field')
    )
  end

  def spatial_cleaner(value)
    if value =~ COORDINATES_REGEXP
      value.sub(COORDINATES_REGEXP, '')[0...-2]
    else
      value
    end
  end

  def type_cleaner(value)
    value.underscore.split('_').collect(&:capitalize).join(' ')
  end

  def format_type_labels(*values)
    values&.flatten.map{|v| type_cleaner v}.join(' OR ')
  end

  def type_labels_for_show_page(values)
    values ||= []
    values = [values] unless values.is_a? Array
    values.map { |v| type_cleaner v }
  end

  def external_identifiers_for_show_page(value)
    return unless value

    identifiers = JSON.parse(value)
    values = json_hash_field(identifiers).html_safe
    values

    rescue JSON::ParserError
  end

  # Render human-readable label for RS.org value, if available and URI otherwise
  def rights_icon_label(uri)
    I18n.t([:rights])[0].each do |r|
      return r[1][:label] if r[1][:uri] == uri
    end
    uri
  end

  # related to show page tabs
  def show_tabs?(document = @document)
    document.fulltext || document.iiif_ids
  end

  def iiif_manifest_urls(document = @document)
    return [] unless @document.iiif_ids

    iiif_prefix = Rails.application.credentials.iiif_prefix.dig(Rails.env.to_sym)
    @document.iiif_ids.map do |id|
      iiif_prefix + id
    end
  end

  def iiif_viewer_iframe_location
    if Rails.env.development?
      embeddable_iiif_viewer_url
    else
      embeddable_iiif_viewer_url protocol: 'https'
    end
  end

  def relabel_blacklight_link(blacklight_link, label)
    return blacklight_link if label.nil?
    link = Nokogiri.parse(blacklight_link).css('a')[0]
    link.inner_html = label
    link.to_s.html_safe
  rescue
    blacklight_link
  end

  def new_search_action
    if FeatureFlags.enabled? :new_search_boxes
      search_records_path
    else
      search_action_url
    end
  end

end
