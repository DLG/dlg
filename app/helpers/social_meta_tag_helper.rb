# frozen_string_literal: true

module SocialMetaTagHelper

  TARGET_ASPECT_RATIO = 21.0/40
  TARGET_WIDTH = 1200

  def img_params
    params.permit('canvas', 'x', 'y', 'w', 'rotate', 'flip')
  end

  def iiif_prefix
    Rails.application.credentials.iiif_prefix.dig(Rails.env.to_sym)
  end

  def canvas_width(canvas_id)
    @document.iiif_widths[canvas_id]&.to_i
  end

  def canvas_height(canvas_id)
    @document.iiif_heights[canvas_id]&.to_i
  end

  # url for og:image meta tag
  def og_image_url
    canvas_id = img_params.key?('canvas') ? img_params['canvas'].to_i : 0
    canvas = @document.iiif_ids[canvas_id]
    return nil if canvas.nil?

    rotate = 0
    if img_params.key? 'rotate'
      rotate = img_params['rotate'].to_i
      rotate = 0 if rotate % 90 > 0
    end
    if img_params.key?('flip') && img_params['flip'] != '0'
      rotate = (rotate + 180) % 360 if rotate % 180 > 0
      transform = "!#{rotate}"
    else
      transform = rotate.to_s
    end

    if img_params.key?('w')
      width = img_params['w'].to_i
      if rotate % 180 == 0
        height = (width * TARGET_ASPECT_RATIO).to_i
      else
        height = width
        width = (height * TARGET_ASPECT_RATIO).to_i
      end
    else
      width = canvas_width(canvas_id)
      height = canvas_height(canvas_id)
    end
    return nil if width.nil? || height.nil?

    x0 = img_params.key?('x') ? [img_params['x'].to_i - width/2, 0].max : 0
    y0 = img_params.key?('y') ? [img_params['y'].to_i - height/2, 0].max : 0
    display_width = [width, TARGET_WIDTH].min
    "#{iiif_prefix}#{canvas}/#{x0},#{y0},#{width},#{height}/#{display_width},/#{transform}/default.jpg"
  end

  def og_description
    @document['dcterms_description_display']&.first
  end

  def og_title
    @document.title
  end

  def og_site_name
    I18n.t('blacklight.application_name')
  end

  def og_image_tag
    url = og_image_url
    return "" if url.nil?
    "<meta property=\"og:image\" content=\"#{url}\" />".html_safe
  rescue
    # Don't let an error with this logic prevent rendering the page
    ""
  end

  def og_url_tag
    url = request.url
    return "" if url.nil?
    "<meta property=\"og:url\" content=\"#{url}\" />".html_safe
  end

  def og_description_tag
    description = og_description
    return "" if description.nil?
    "<meta property=\"og:description\" content=\"#{h(description)}\" />".html_safe
  end

  def og_title_tag
    return "" if og_title.nil?
    "<meta property=\"og:title\" content=\"#{h(og_title)}\" />".html_safe
  end

  def og_site_name_tag
    return "" if og_site_name.nil?
    "<meta property=\"og:site_name\" content=\"#{h(og_site_name)}\" />".html_safe
  end

end
