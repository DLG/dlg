# frozen_string_literal: true

# helper methods for RecordsController views
module RecordsHelper
  def set_page_title
    prefix = if @collection && !has_search_parameters?
               "#{@collection.display_title} Collection Items"
             elsif @collection && has_search_parameters?
               "#{render_search_to_page_title(params)} - #{@collection.display_title} Collection Items"
             end
    @page_title = "#{prefix} - #{application_name}"
  end

  def collection_limited_results?
    @collection && @response.documents
  end

  def serial_limited_results?
    @serial && @response.documents
  end

  def multivalued_collection_field(field, connector = ', ', linkify: false)
    return '' unless field&.any?

    field = field.map { |value| find_urls_and_linkify(value)} if linkify

    field.map.to_a.to_sentence(two_words_connector: connector).html_safe
  end

  def collection_field_links(field_values, facet)
    return [] unless field_values&.any?

    links = field_values.map do |val|
      link_to(val, search_action_path(search_state.add_facet_params_and_redirect(facet, val)))
    end
    links.to_sentence.html_safe
  end

  def rights_statement_icons(dc_right)
    return '' unless dc_right&.any?

    image_tags = []
    I18n.t([:rights])[0].each do |r|
      dc_right.each do |rights|
        next unless r[1][:uri] == rights

        image_tags << rights_statement_icon_link(r)
      end
    end
    image_tags.join('').html_safe
  end

  def more_about_field_tag(label, field_value)
    tag.p(tag.strong(label) + tag.br + field_value, class: label.parameterize.underscore) if field_value.present?
  end

  def name_letter_pressed?(letter, params)
    # Letter should never be pressed when query is present
    params['q'].blank? && (
      (params[:first_char].blank? && letter == @a_to_z.without('0-9').first) ||
        params[:first_char]&.upcase == letter
    )
  end

  def formatted_manifest_url(document)
    return document.iiif_manifest_url unless document.iiif_ids&.any?

    path = URI(document.iiif_manifest_url).path
    if Rails.env.development?
      URI::HTTP.build(host: request.host, port: request.port, path: path).to_s
    else
      URI::HTTPS.build(host: request.host, path: path).to_s
    end
  end

  def json_hash_field(field)
    return '' unless field

    tags = field.map do |identifier|
      content_tag(:span, identifier['type'], class: 'badge badge-primary') + ' ' + identifier['value']
    end

    tags.map.to_a.to_sentence.html_safe
  end
end
