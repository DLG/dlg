# frozen_string_literal: true

class FeatureFlags
  def self.enabled?(feature_name)
    case feature_name.to_sym
    when :sensitive_content_field
      !Rails.env.production?
    when :twitter_feed
      false
    when :serials
      true
    when :new_search_boxes
      !Rails.env.production?
    else
      true
    end
  end
end

