# frozen_string_literal: true

# :nodoc:
class FeatureService
  class FeatureServiceError < StandardError; end

  DEFAULT_FQ = "+class_name_ss:Feature
                +display_b:1
                +portals_sms:georgia
               ".squish.freeze

  # @return [Array<FeatureSolrDocument>] Carousel Features
  def self.features(area = nil)

    @conn = Blacklight.default_index.connection
    raise StandardError, 'Blacklight not initialized' unless @conn

    fq = if area
           DEFAULT_FQ + " +area_ss:#{area}"
         else
           DEFAULT_FQ
         end
    response = @conn.get 'select', params: {
      fq: fq,
      rows: 1000
    }
    return [] if response['response']['docs'].empty?

    response['response']['docs'].map do |doc|
      FeatureSolrDocument.new doc
    end
  end
end
