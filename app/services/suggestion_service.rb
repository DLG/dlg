# frozen_string_literal: true

# :nodoc:
class SuggestionService
  def self.suggestions(facet_name, query, search_builder = RecordSearchBuilder)
    suggestions = FacetService.suggest_facet_values(facet_name,
                                                    query,
                                                    *search_builder.record_filters)
    suggestions.map do |suggestion|
      value = suggestion.first
      count = suggestion.last
      {
        value: value,
        text: "#{value}  (#{count})"
      }
    end
  end
end