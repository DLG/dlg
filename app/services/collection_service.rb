# frozen_string_literal: true

# :nodoc:
class CollectionService < SolrService
  # @return [Array<CollectionSolrDocument>]
  def self.by_record_ids(record_ids, *additional_filters, field_list: nil)
    filters = CollectionSearchBuilder.filters + additional_filters
    response = get_documents record_ids, *filters, solr_column: SolrDocument.unique_key, field_list: field_list
    get_solr_docs response, CollectionSolrDocument
  end

  # @return [Array<CollectionSolrDocument>]
  def self.by_slug(slugs, *additional_filters, field_list: nil)
    filters = CollectionSearchBuilder.filters + additional_filters
    response = get_documents slugs, *filters, solr_column: CollectionSolrDocument.slug, field_list: field_list
    get_solr_docs response, CollectionSolrDocument
  end

  # @return [Array<CollectionSolrDocument>]
  def self.by_institution_slugs(inst_slugs, *additional_filters, field_list: nil)
    filters = CollectionSearchBuilder.filters + additional_filters
    response = get_documents inst_slugs, *filters, solr_column: CollectionSolrDocument.institution_slugs, field_list: field_list
    get_solr_docs response, CollectionSolrDocument
  end

  def self.institution_slug_hash(inst_slugs, field_list: 'record_id_ss,display_title_display,institution_slug_sms')
    docs = by_institution_slugs(inst_slugs, field_list: field_list)
    inst_hash = {}
    docs.each do |collection|
      collection.institution_slugs.each do |slug|
        if inst_hash.key?(slug)
          inst_hash[slug] = (inst_hash[slug] << collection).uniq.sort_by(&:display_title)
        else
          inst_hash[slug] = [collection]
        end
      end
    end
    inst_hash
  end
end