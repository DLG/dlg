# frozen_string_literal: true

# :nodoc:
class SerialService < SolrService
  # @return [Array<CollectionSolrDocument>]
  def self.by_record_ids(record_ids, *additional_filters, field_list: nil)
    filters = SerialsSearchBuilder.filters + additional_filters
    response = get_documents record_ids, *filters, solr_column: SerialSolrDocument.unique_key, field_list: field_list
    get_solr_docs response, SerialSolrDocument
  end
end