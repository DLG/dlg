# frozen_string_literal: true

# :nodoc:
class FacetService
  def self.get_facet_counts(facet_name, *filters)
    query = {
      'fq' => filters,
      'rows' => 0,
      'facet' => true,
      'facet.field' => facet_name,
      'facet.limit' => 20_000
    }
    response = Blacklight.default_index.connection.get 'select', params: query
    response&.dig('facet_counts', 'facet_fields', facet_name)&.each_slice(2)&.to_h
  end

  def self.search_facet_values(facet_name, starts_with, contains, *filters)
    query = {
      'fq' => filters,
      'rows' => 0,
      'facet' => true,
      'facet.field' => facet_name,
      'facet.limit' => 10
    }
    if starts_with.present?
      query['facet.prefix'] = starts_with # this is unfortunately case-sensitive
    end
    if contains.present?
      query['facet.contains'] = contains
      query['facet.contains.ignoreCase'] = true
    end
    response = Blacklight.default_index.connection.get 'select', params: query
    response&.dig('facet_counts', 'facet_fields', facet_name)&.each_slice(2)&.to_h
  end

  # This tries to strike a balance between the case-sensitive prefix and the case-insensitive contains search options.
  # It will only work with facets where values generally begin with a capital letter.
  def self.suggest_facet_values(facet_name, search_term, *filters)
    first_let = search_term.first.upcase
    results = results_with_first_let = search_facet_values facet_name, first_let, search_term, *filters
    if results.size < 10 || search_term.size > 4
      results_without_first_let = search_facet_values facet_name, nil, search_term, *filters
      results = results_without_first_let.merge results
    end
    # Rank results with the first-letter match 10x higher
    results.sort_by{|k, v| results_with_first_let.include? k ? -v : -v/10 }[0..9]
  end

  def self.get_items_facet_counts(facet_name, *additional_filters)
    filters = RecordSearchBuilder.item_filters + additional_filters
    get_facet_counts facet_name, *filters
  end

  def self.get_records_facet_counts(facet_name, *additional_filters)
    filters = RecordSearchBuilder.record_filters + additional_filters
    get_facet_counts facet_name, *filters
  end

end
