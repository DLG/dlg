# frozen_string_literal: true

# :nodoc:
class SolrService
  def self.get_documents(record_ids, *filters, solr_column: 'record_id_ss', field_list: nil, start: 0, rows: 1000 )
    ids_string = prepare_record_ids(record_ids)
    return if ids_string.blank?

    fq_filters = ["#{solr_column}:#{ids_string}"] + filters
    query = {
      'fq' => fq_filters,
      'start' => start,
      'rows' => rows
    }
    query['fl'] = field_list if field_list.present?
    Blacklight.default_index.connection.get 'select', params: query
  end

  def self.get_solr_docs(response, solr_doc_class = SolrDocument)
    return [] if response.nil? || (response.dig('response', 'docs') && response.dig('response', 'docs').empty?)

    response.dig('response', 'docs').map do |doc|
      solr_doc_class.new doc
    end
  end

  def self.prepare_record_ids(*record_ids)
    id_string = record_ids.flatten.reject(&:blank?).join(' OR ')
    if id_string.present?
      "(#{id_string})"
    else
      ''
    end
  end
end
