# frozen_string_literal: true

# helper methods for IIIF manifests
module ManifestHelper
  extend ActiveSupport::Concern
  include Rails.application.routes.url_helpers
  include ActionView::Helpers::AssetUrlHelper
  include CatalogHelper
  include LinkingHelper
  include ThumbnailHelper
  require 'httparty'

  # @param [SolrDocument] document
  def generate_presentation(document)
    {
      '@context': 'http://iiif.io/api/presentation/3/context.json',
      id: document.iiif_manifest_url,
      type: 'Manifest',
      label: generate_presentation_label(document),
      attribution: generate_attribution(document),
      logo: cache_server_image_link(document.holding_institution_image),
      license: document['dc_right_display']&.first,
      seeAlso: see_also(document),
      metadata: generate_metadata(document),
      description: document['dcterms_description_display']&.first,
      items: generate_items(document)
    }
  end

  # @param [SolrDocument] document
  def generate_presentation_label(document)
    title = document.title
    title += ", #{document.collection_title}" unless document.collection_title.blank?
    if document[:dcterms_provenance_display]&.first.present?
      title += ", #{document[:dcterms_provenance_display]&.first}"
    end
    { 'en': [title] }
  end

  def generate_attribution(document)
    "#{rights_statement(document)}<br>#{document.iiif_attribution}"
  end

  def see_also(document)
    { '@id': solr_document_url(document.id, format: 'json') }
  end

  # @param [SolrDocument] document
  def generate_metadata(document)
    [
      metadatum('Title', document.title),
      metadatum('Holding Institution', document[:dcterms_provenance_display]&.first),
      metadatum('Online Collection', document.collection_title),
      metadatum('Creator', document['dcterms_creator_display']&.join('<br>')),
      metadatum('Contributor', document['dcterms_contributor_display']&.join('<br>')),
      metadatum('Publisher', document['dcterms_publisher_display']&.join('<br>')),
      metadatum('Date', document['dc_date_display']&.join('<br>')),
      metadatum('Subject', document['dcterms_subject_display']&.join('<br>')),
      metadatum('Personal Subject', document['dlg_subject_personal_display']),
      metadatum('Location', document['dcterms_spatial_display']),
      metadatum('Temporal coverage', document['dcterms_temporal_display']),
      metadatum('Medium', document['dcterms_medium_display']),
      metadatum('Type', document['dcterms_type_display']),
      metadatum('File format', document['dc_format_display']),
      metadatum('Description', document['dcterms_description_display']&.join('<br>')),
      metadatum('Extent', document['dcterms_extent_display']),
      metadatum('Local identifier', document['dcterms_identifier_display']),
      metadatum('DLG record ID', document['record_id_ss']),
      metadatum('Metadata URL', document['edm_is_shown_at_display']),
      metadatum('Digital Object URL', document['edm_is_shown_by_display']),
      metadatum('Original collection', document['dcterms_is_part_of_display']),
      metadatum('Citation', document['dcterms_bibliographic_citation_display']),
      metadatum('Related materials', document['dc_relation_display']),
      metadatum('Language', document['dcterms_language_display']),
      metadatum('Rights', rights_statement(document)),
      metadatum('Portal', document['portal_names_sms'])
    ].compact
  end
  
  # @param [String] label
  # @param [Array<String>] value
  def metadatum(label, value)
    return nil if value.blank?

    { label: label, value: value }
  end

  # @param [SolrDocument] document
  def rights_statement(document)
    rights = rights_image_tag(document).to_s
    rights += "<br>#{document['dcterms_rights_holder_display'].first}".html_safe unless document['dcterms_rights_holder_display'].blank?
    rights += "<br>#{document['dlg_local_right_display'].first}".html_safe unless document['dlg_local_right_display'].blank?
    rights
  end

  # @param [String] document
  def rights_image_tag(document)
    I18n.t([:rights])[0].each do |r|
      if r[1][:uri] == document['dc_right_display']&.first
        image_url = ActionController::Base.helpers.image_url(r[1][:icon_url], host: root_url)
        return ActionController::Base.helpers.link_to ActionController::Base.helpers.image_tag(image_url, alt:'Rights Statement information'), r[1][:uri]
      end
    end
    document['dc_right_display']&.first
  end

  def convert_size(size_array, index)
    if size_array.nil? || size_array.empty? || size_array[index].blank?
      return 800
    end

    converted = size_array[index].to_i
    if converted > 0
      converted
    else
      800
    end
  end

  # @param [SolrDocument] document
  def generate_items(document)
    iiif_manifest_urls(document).each_with_index.map do |manifest_url, idx|
      case document.iiif_item_types&.dig idx
      when 'image'
        generate_image_item manifest_url, document, idx
      when nil
        generate_image_item manifest_url, document, idx
      when 'video'
        generate_video_item document, idx
      when 'audio'
        generate_audio_item document, idx
      else
        {}
      end
    end
  end

  def generate_image_item(manifest_url, document, idx)
    {
      id: "#{solr_document_url(document)}/canvas/#{idx + 1}",
      type: 'Canvas',
      label: { en: [document.iiif_titles[idx]] },
      height: document.iiif_heights[idx],
      width: document.iiif_widths[idx],
      items: [{
        id: "#{solr_document_url(document)}/canvas/#{idx + 1}/1",
        type: 'AnnotationPage',
        items: [{
          id: "#{solr_document_url(document)}/annotation/#{idx + 1}/1",
          type: 'Annotation',
          motivation: 'painting',
          body: {
            id: manifest_url,
            type: 'Image',
            format: "image/#{document.iiif_file_types&.dig(idx) || 'jpeg'}",
            height: document.iiif_heights[idx],
            width: document.iiif_widths[idx],
            service: {
              id: manifest_url,
              type: 'ImageService2',
              profile: 'level1'
            }
          },
          target: "#{solr_document_url(document)}/canvas/#{idx + 1}"
        }]
      }],
      rendering: [
        {
          '@id': "#{manifest_url}/full/full/0/default.jpg?download=true",
          label: full_download_label(document.iiif_widths[idx], document.iiif_heights[idx]),
          format: 'image/jpg'
        }
      ]
    }
  end

  def full_download_label(width, height)
    label = "Full size image"
    if width.present? && height.present? && width.to_i > 0 && height.to_i > 0
      # these should always be there, but the viewer will still sort of work without a width and height
      # so we'll hide them in this label in the rare case where they're not set
      label += " (#{width} x #{height}px)"
    end
    label
  end

  def generate_video_item(document, idx)
    item = {
      id: "#{solr_document_url(document)}/canvas/#{idx + 1}",
      type: 'Canvas',
      label: { en: [document.iiif_titles[idx]] },
      height: document.iiif_heights[idx],
      width: document.iiif_widths[idx],
      duration: document.iiif_durations[idx],
      items: [{
        id: "#{solr_document_url(document)}/canvas/#{idx + 1}/1",
        type: 'AnnotationPage',
        items: [{
          id: "#{solr_document_url(document)}/annotation/#{idx + 1}/1",
          type: 'Annotation',
          motivation: 'painting',
          body: {
            id: document.iiif_media_urls[idx],
            type: 'Video',
            format: "video/#{document.iiif_file_types[idx]}",
            height: document.iiif_heights[idx],
            width: document.iiif_widths[idx],
            duration: document.iiif_durations[idx]
          },
          target: "#{solr_document_url(document)}/canvas/#{idx + 1}"
        }]
      }]
    }
    item[:thumbnail] = document.iiif_media_thumbnail_urls[idx] if document.iiif_media_thumbnail_urls&.dig(idx).present?
    item
  end

  def generate_audio_item(document, idx)
    item = {
      id: "#{solr_document_url(document)}/canvas/#{idx + 1}",
      type: 'Canvas',
      label: { en: [document.iiif_titles[idx]] },
      duration: document.iiif_durations[idx],
      items: [{
        id: "#{solr_document_url(document)}/canvas/#{idx + 1}/1",
        type: 'AnnotationPage',
        items: [{
          id: "#{solr_document_url(document)}/annotation/#{idx + 1}/1",
          type: 'Annotation',
          motivation: 'painting',
          body: {
            id: document.iiif_media_urls[idx],
            type: 'Sound',
            format: "audio/#{document.iiif_file_types[idx]}",
            duration: document.iiif_durations[idx]
          },
          target: "#{solr_document_url(document)}/canvas/#{idx + 1}"
        }]
      }]
    }
    item[:thumbnail] = document.iiif_media_thumbnail_urls[idx] if document.iiif_media_thumbnail_urls&.dig(idx).present?
    item
  end
end
