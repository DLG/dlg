# frozen_string_literal: true

# helper to easily configure blacklight to get documents for show pages using a custom query
# This can be used to choose a custom slug field or apply extra facets
module HasCustomSolrGetter
  extend ActiveSupport::Concern

  included do
    def self.custom_solr_getter(blacklight_config, slug_field:, fq: [])
      blacklight_config.document_unique_id_param = 'slug' # arbitrary name, doesn't need to match your slug field
      blacklight_config.document_solr_path = 'select'
      blacklight_config.default_document_solr_params = {
        fq: fq,
        q: "{!raw f=#{slug_field} v=$slug}", # the v=$... matches document_unique_id_param above
        rows: 1,
        omitHeader: true,
        facet: false
      }
    end
  end
end

