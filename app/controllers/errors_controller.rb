# frozen_string_literal: true

# Error page static content controller
class ErrorsController < HomepageController

  before_action :set_format
  def file_not_found
    render status: 404
  end

  def unprocessable
    render status: 422
  end

  def internal_server_error
    render status: 500
  end

  private

  def set_format
    request.format = 'html'
  end
end
