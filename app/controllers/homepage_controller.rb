# frozen_string_literal: true

# Serve up the homepage!
class HomepageController < CatalogController
  layout 'homepage'
  def index
    set_features
  end

  # return link to faceted main results page
  def search_action_url(options = {})
    search_records_path(options.except(:controller, :action))
  end

  private

  def set_features
    features = FeatureService.features
    @tabs_primary_feature = features.select(&:primary_tab?).sort_by(&:created_at).reverse.first
    @tabs_side_features = features.select(&:side_tab?).sample(3)
  end
end