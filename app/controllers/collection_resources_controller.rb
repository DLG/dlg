# frozen_string_literal: true

# Collection Resource controller
class CollectionResourcesController < ErrorsController
  def show
    @collection = CollectionService.by_record_ids(params['collection_record_id']).first
    @resource = @collection.collection_resources.select do |resource|
      resource['slug'].downcase == params[:collection_resource_slug].downcase
    end&.first

    raise ActionController::RoutingError, 'Not Found' unless @collection && @resource
  end
end
