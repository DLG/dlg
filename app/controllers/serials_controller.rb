# frozen_string_literal: true

class SerialsController < CatalogController
  include BlacklightMaps::Controller
  include BlacklightRangeLimit::ControllerOverride

  configure_blacklight do |config|
    config.search_builder_class = SerialsSearchBuilder
    config.document_model = SerialSolrDocument

    # Facets
    config.add_facet_field :creator_facet, label: I18n.t('search.facets.creator'), limit: true
    config.add_facet_field :subject_facet, label: I18n.t('search.facets.subject'), limit: true
    config.add_facet_field :location_facet, label: I18n.t('search.facets.location'), limit: true, helper_method: :spatial_cleaner
    config.add_facet_field :counties_facet, label: I18n.t('search.facets.county'), limit: true
    config.add_facet_field :year_facet,    label: I18n.t('search.facets.year'), range: true
    config.add_facet_field :medium_facet,  label: I18n.t('search.facets.medium'), limit: true

    # Results Fields
    config.add_index_field :description, label: I18n.t('search.labels.short_description'), helper_method: :strip_html
    config.add_index_field :dc_date_display, label: I18n.t('search.labels.dc_date')

    # Show Fields
    config.add_show_field :dcterms_publisher_display, label: I18n.t('search.labels.dcterms_publisher')
    config.add_show_field :dc_date_display, label: I18n.t('search.labels.dc_date')
    config.add_show_field :dcterms_subject_display, label: I18n.t('search.labels.dcterms_subject'), link_to_search: :subject_facet
    config.add_show_field :dcterms_subject_fast_display, label: I18n.t('search.labels.dcterms_subject_fast'), link_to_search: :subject_facet
    config.add_show_field :dcterms_spatial_display, label: I18n.t('search.labels.dcterms_spatial'), link_to_search: :location_facet
    config.add_show_field :dcterms_medium_display, label: I18n.t('search.labels.dcterms_medium'), link_to_search: :medium_facet
    config.add_show_field :dcterms_language_display, label: I18n.t('search.labels.dcterms_language')
    config.add_show_field :dcterms_frequency_display, label: I18n.t('search.labels.dcterms_language')
    config.add_show_field :dcterms_description_display, label: I18n.t('search.labels.dcterms_description'), helper_method: :linkify_urls
    config.add_show_field :dcterms_is_replaced_by, label: I18n.t('search.labels.dcterms_is_replaced_by')

    config.show.html_title = 'title_display'

  end

  def search_action_url(options = {})
    search_serials_path(options.except(:controller, :action))
  end
end
