# frozen_string_literal: true

# handle actions for Institution browse page
class InstitutionsController < CatalogController
  include HasCustomSolrGetter
  before_action :set_item_counts, only: [:index]
  before_action :set_a_to_z, only: [:index]
  before_action :set_first_character, only: [:index]
  before_action :set_collections_item_counts, only: [:index]

  layout 'blacklight'

  configure_blacklight do |config|
    config.search_builder_class = InstitutionSearchBuilder
    config.document_model = InstitutionSolrDocument
    config.show.route = { controller: 'institutions' }

    custom_solr_getter config, slug_field: InstitutionSolrDocument.unique_key, fq: InstitutionSearchBuilder.filters


    # solr fields to be displayed in the index (search results) view
    #   The ordering of the field names is the order of the display
    config.add_index_field :authorized_name,      label: I18n.t('institution.fields.authorized_name')
    config.add_facet_field 'first_char_ss', label: 'A-Z', limit: 50, sort: 'index'

    # Show Fields
    config.show.html_title_field = 'title'

    config.add_show_field :authorized_name, label: I18n.t('institution.fields.authorized_name')
  end

  def index
    super
    @institution_collections = CollectionService.institution_slug_hash(@response.docs.map(&:slug))
  end

  def show
    super
    @institution_collections = CollectionService.institution_slug_hash(@response.docs.map(&:slug))
  end

  def search_action_url(options = {})
    search_records_path(options.except(:controller, :action))
  end

  private

  def set_first_character
    if params['q'].present?
      @search_state.params.delete('f')
      return
    end

    if params[:first_char]
      @search_state.params.merge!(f: { first_char_ss: [params[:first_char].upcase] })
    else
      @search_state.params.merge!(f: { first_char_ss: [@a_to_z.without('0-9').first] })
    end
  end

  def set_item_counts
    @item_counts = FacetService.get_items_facet_counts('provenance_facet')
  end

  def set_collections_item_counts
    @collections_item_counts = FacetService.get_records_facet_counts'provenance_facet', '{!join from=collection_record_id_sms to=id}class_name_ss:Item display_b:1 portals_sms:georgia'
  end

  def set_a_to_z
    @a_to_z = @item_counts.keys.map {|name| first_char(name) }.uniq.sort
  end

  def first_char(name)
    first_char = name[0]
    if first_char.match(/^[0-9]$/)
      '0-9'
    else
      first_char.upcase
    end
  end

end
