# frozen_string_literal: true

# Main Blacklight controller housing common configuration shared with other
# child Controllers
class CatalogController < ApplicationController
  include Blacklight::Catalog

  layout 'blacklight'

  configure_blacklight do |config|

    # Default parameters to send to solr for all search-like requests.
    # See also SearchBuilder#processed_parameters
    config.default_solr_params = { qt: 'search' }

    # TODO: facets are defined in query handler so this should not be needed
    # makes debugging solr calls easier
    config.add_facet_fields_to_solr_request!

    # set maximum results per page (experimental)
    config.max_per_page = 250

    # items to show per page, each number in the array represent another option to choose from.
    config.per_page = [10, 20, 50, 100]

    # solr field configuration for search results/index views
    config.index.title_field = 'title'
    config.index.display_type_field = 'class_name_ss'

    # solr field configuration for document/show views
    config.show.title_field = 'title'
    config.show.display_type_field = 'class_name_ss'

    # show thumbnails on search results for most view types
    # now handled in templates, see _show_item, _thumbnail_item, etc.
    config.index.thumbnail_method = :thumbnail_image_tag

    config.index.document_component = DlgDocumentComponent
    config.index.constraints_component = DlgConstraintsComponent

    config.view.gallery.document_component = DlgGalleryDocumentComponent
    config.view.gallery.default = true

    # fulltexty
    config.add_search_field('both') do |field|
      field.include_in_advanced_search = false
      field.include_in_simple_select = true
      field.label = I18n.t('search.labels.both')
      field.solr_local_parameters = {
        qf: 'title_unstem_search^50
             dcterms_title_text^20
             fulltext_texts^10
             creator_unstem_search^10
             contributor_unstem_search^10
             subject_unstem_search^10
             subject_personal_unstem_search^10
             description_unstem_search^10
             provenance_unstem_search^10
             publisher_unstem_search^10
             date_unstem_search^10
             temporal_unstem_search^10
             spatial_unstem_search^10
             is_part_of_unstem_search^10
             identifier_unstem_search^10
             edm_is_shown_at_text^10
             edm_is_shown_by_text^10
             collection_titles_unstem_search^10
             dc_right_unstem_search^5
             dc_right_text
             dcterms_creator_text
             dcterms_contributor_text
             dcterms_subject_text
             dlg_subject_personal_text
             dcterms_subject_fast_text
             dcterms_description_text
             dcterms_provenance_text
             dcterms_publisher_text
             dcterms_temporal_text
             dcterms_spatial_text
             dcterms_is_part_of_text
             dcterms_identifier_text
             dcterms_replaces_text
             dcterms_is_replaced_by_text
             collection_titles_text'.squish,
        pf: 'title_unstem_search^100
             fulltext_texts^50
             dcterms_title_text^20
             creator_unstem_search^20
             contributor_unstem_search^20
             subject_unstem_search^20
             subject_personal_unstem_search^20
             description_unstem_search^20
             provenance_unstem_search^20
             publisher_unstem_search^20
             date_unstem_search^20
             temporal_unstem_search^20
             spatial_unstem_search^20
             is_part_of_unstem_search^20
             identifier_unstem_search^20
             edm_is_shown_at_text^20
             edm_is_shown_by_text^20
             collection_titles_unstem_search^20
             dc_right_unstem_search^10
             dc_right_text^5
             dcterms_creator_text^5
             dcterms_contributor_text^5
             dcterms_subject_text^5
             dlg_subject_personal_text^5
             dcterms_subject_fast_text^5
             dcterms_description_text^5
             dcterms_provenance_text^5
             dcterms_publisher_text^5
             dcterms_temporal_text^5
             dcterms_spatial_text^5
             dcterms_is_part_of_text^5
             dcterms_identifier_text^5
             dcterms_replaces_text^5
             dcterms_is_replaced_by_text^5
             collection_titles_text^5'.squish
      }
    end

    config.add_search_field('metadata') do |field|
      field.label = I18n.t('search.labels.metadata')
      field.include_in_simple_select = true
      field.include_in_advanced_search = true
    end

    config.add_search_field('fulltext') do |field|
      field.label = I18n.t('search.labels.fulltext')
      field.include_in_advanced_search = true
      field.include_in_simple_select = true
      field.solr_local_parameters = {
        qf: 'fulltext_texts^1000',
        pf: 'fulltext_texts^1000'
      }
    end

    # If there are more than this many search results, no spelling ("did you
    # mean") suggestion is offered.
    config.spell_max = 5

    # Configuration for autocomplete suggestor
    config.autocomplete_enabled = false
    config.autocomplete_path = 'suggest'

    # Blacklight update to 7.x
    config.add_results_document_tool(:bookmark, partial: 'bookmark_control', if: :render_bookmarks_control?)

    config.add_results_collection_tool(:sort_widget)
    config.add_results_collection_tool(:per_page_widget)
    config.add_results_collection_tool(:view_type_group)

    config.add_show_tools_partial(:bookmark, partial: 'bookmark_control', if: :render_bookmarks_control?)
    config.add_show_tools_partial(:email, callback: :email_action, validator: :validate_email_params)
    config.add_show_tools_partial(:sms, if: :render_sms_action?, callback: :sms_action, validator: :validate_sms_params)
    config.add_show_tools_partial(:citation)

    config.add_nav_action(:bookmark, partial: 'blacklight/nav/bookmark', if: :render_bookmarks_control?)
    config.add_nav_action(:search_history, partial: 'blacklight/nav/search_history')

    # add Admin and Search menu to navbar
    config.add_nav_action :search
    config.add_nav_action :admin

  end

  def sms_mappings
    nil # returning nil tells Blacklight to hide SMS action
  end

  # note: some controllers override this
  def search_action_url(options = {})
    super(options.except(:controller, :action).merge({only_path: true}))
  end

  private

  def current_search_session
    super
  rescue ActiveRecord::ActiveRecordError
    nil
  end

end
