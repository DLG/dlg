class ContactController < HomepageController

  def index
    @formtype = contact_params[:formtype]||'dlg'
    @page_title = I18n.t('titles.contact_us', application_name: I18n.t('blacklight.application_name'))
  end

  def contact_params
    params.permit(:formtype)
  end

end
