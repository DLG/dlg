# frozen_string_literal: true

# "About" menu items static content controller
class AboutController < HomepageController
  def mission; end

  def policy; end

  def partners_sponsors; end

  def api; end

  def harmful_content; end

  def harmful_content_form; end

  def harmful_content_submit
    mail = HarmfulContentMailer.harmful_content_submission harmful_content_params
    sent = if mail.respond_to? :deliver_now
             mail.deliver_now
           else
             mail.deliver
           end
    respond_to do |format|
      format.html do
        if sent
          flash[:notice] = I18n.t('mail.harmful_content.status.sent')
        else
          flash[:error] = I18n.t('mail.harmful_content.status.failed')
        end
        redirect_to about_harmful_content_path
      end
    end
  end

  def harmful_content_params
    params.permit(:item_url, :concern, :note, :name, :email)
  end
end
