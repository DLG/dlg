# Controller for the dedicated Mirador page
class IiifViewerController < HomepageController
  layout 'homepage'
  def iiif_viewer
    @manifest = iiif_viewer_params[:manifest]
    end

  def embeddable_iiif_viewer
    response.delete_header('X-Frame-Options')
    @manifest = iiif_viewer_params[:manifest]
    @allow_image_comparison = iiif_viewer_params.key? :full
    render :iframe_iiif_viewer, layout: false
  end

  def iiif_viewer_params
    params.permit(:manifest, :full)
  end
end
