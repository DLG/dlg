# frozen_string_literal: true

# Represent an Event from Solr
class CollectionSolrDocument < SolrDocument

  self.unique_key = 'record_id_ss'
  def self.slug
    'slug_ss'
  end

  def self.institution_slugs
    'institution_slug_sms'
  end

  # @return [String]
  def slug
    self[:slug_ss]
  end

  # @return [String]
  def record_id
    self[:record_id_ss]
  end

  # @return [String]
  def short_description
    self['short_description_display']&.first
  end

  # @return [String]
  def display_title
    self['display_title_display']&.first
  end

  # @return [Array<String>] public_collections
  def collection_resources
    return [] unless self[:collection_resources_texts].present?

    JSON.parse self[:collection_resources_texts].first
  end

  # @return [String] sponsor_note
  def sponsor_note
    self['sponsor_note_ss']
  end

  # @return [String] sponsor_image
  def sponsor_image
    self['sponsor_image_ss']
  end

  # @return [String] sensitive_content
  def sensitive_content
    self['sensitive_content_display']
  end

  def institution_slugs
    self['institution_slug_sms']
  end
end
