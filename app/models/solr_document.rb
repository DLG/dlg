# frozen_string_literal: true

class SolrDocument
  include Blacklight::Solr::Document
  include Blacklight::Gallery::OpenseadragonSolrDocument
  include CitationExports
  # self.unique_key = 'id'
  # Email uses the semantic field mappings below to generate the body of an email.
  SolrDocument.use_extension(Blacklight::Document::Email)
  # SMS uses the semantic field mappings below to generate the body of an SMS email.
  SolrDocument.use_extension(Blacklight::Document::Sms)
  # DublinCore uses the semantic field mappings below to assemble an OAI-compliant Dublin Core document
  # Semantic mappings of solr stored fields. Fields may be multi or
  # single valued. See Blacklight::Document::SemanticFields#field_semantics
  # and Blacklight::Document::SemanticFields#to_semantic_values
  # Recommendation: Use field names from Dublin Core
  use_extension(Blacklight::Document::DublinCore)

  PUBLIC_DOMAIN_DC_RIGHTS = %w[
    http://rightsstatements.org/vocab/NKC/1.0/
    http://rightsstatements.org/vocab/NoC-US/1.0/
    https://creativecommons.org/publicdomain/zero/1.0/
  ].freeze

  DOWNLOADABLE_DC_RIGHTS = %w[
    http://rightsstatements.org/vocab/NKC/1.0/
    http://rightsstatements.org/vocab/NoC-US/1.0/
    http://rightsstatements.org/vocab/InC-NC/1.0/
    http://rightsstatements.org/vocab/NoC-NC/1.0/
    https://creativecommons.org/licenses/by-nc/4.0/
    https://creativecommons.org/licenses/by/4.0/
  ].freeze

  field_semantics.merge!(
    contributor: 'dcterms_contributor_display',
    coverage: 'dcterms_spatial_display',
    creator: 'dcterms_creator_display',
    date: 'dc_date_display',
    description: 'dcterms_description_display',
    provenance: 'dcterms_provenance_display',
    format: 'dc_format_display',
    identifier: 'dcterms_identifier_display',
    language: 'dcterms_language_display',
    publisher: 'dcterms_publisher_display',
    relation: 'dc_relation_display',
    rights: 'dc_right_display',
    source: 'dcterms_is_part_of_display',
    subject: 'dcterms_subject_display',
    title: 'dcterms_title_display',
    type: 'dcterms_type_display'
  )

  def as_json(_)
    {
      id: id,
      title: title,
      collection_id: collection_id,
      collection_title: collection_title,
      dcterms_contributor: self['dcterms_contributor_display'],
      dcterms_spatial: self['dcterms_spatial_display'],
      dcterms_creator: self['dcterms_creator_display'],
      dc_date: self['dc_date_display'],
      dcterms_description: self['dcterms_description_display'],
      dc_format: self['dc_format_display'],
      dcterms_identifier: self['dcterms_identifier_display'],
      dcterms_language: self['dcterms_language_display'],
      dcterms_publisher: self['dcterms_publisher_display'],
      dc_relation: self['dc_relation_display'],
      dc_right: self['dc_right_display'],
      dcterms_is_part_of: self['dcterms_is_part_of_display'],
      dcterms_subject: self['dcterms_subject_display'],
      dcterms_title: self['dcterms_title_display'],
      dcterms_type: self['dcterms_type_display'],
      dcterms_provenance: self['dcterms_provenance_display'],
      edm_is_shown_by: self['edm_is_shown_by_display'],
      edm_is_shown_at: self['edm_is_shown_at_display'],
      dcterms_temporal: self['dcterms_temporal_display'],
      dcterms_rights_holder: self['dcterms_rights_holder_display'],
      dcterms_bibliographic_citation: self['dcterms_bibliographic_citation_display'],
      dlg_local_right: self['dlg_local_right_display'],
      dcterms_medium: self['dcterms_medium_display'],
      dcterms_extent: self['dcterms_extent_display'],
      dlg_subject_personal: self['dlg_subject_personal_display'],
      iiif_manifest_url_ss: self['iiif_manifest_url_ss'],
      fulltext: fulltext
    }
  end

  def klass
    self[:class_name_ss]
  end

  def item?
    klass == 'Item'
  end

  def collection?
    klass == 'Collection'
  end

  def serial?
    klass == 'Serial'
  end

  def title
    self[:title]
  end

  def image
    self[:image_ss]
  end

  def thumbnail
    self[:thumbnail_ss]
  end

  def fulltext
    texts = self[:fulltext_texts]
    texts.is_a?(Array) ? texts.reject(&:blank?).first : texts
  end

  def iiif_ids
    self[:iiif_ids_sms]
  end

  def iiif_item_types
    self[:iiif_item_types_sms]
  end

  def iiif_file_types
    self[:iiif_file_types_sms]
  end

  def iiif_media_urls
    self[:iiif_media_urls_sms]
  end

  def iiif_media_thumbnail_urls
    self[:iiif_media_thumbnail_urls_sms]
  end

  def iiif_titles
    self[:iiif_titles_sms]
  end

  def iiif_heights
    self[:iiif_heights_sms]
  end

  def iiif_widths
    self[:iiif_widths_sms]
  end

  def iiif_durations
    self[:iiif_durations_sms]
  end

  def iiif_manifest_url
    self[:iiif_manifest_url_ss]
  end

  def iiif_attribution
    self[:iiif_attribution_ss]
  end

  def holding_institution_image
    self[:holding_institution_image_ss]
  end

  def types
    self[:dcterms_type_display]
  end

  def do_url
    self[:edm_is_shown_by_display]&.first
  end

  def md_url
    self[:edm_is_shown_at_display]&.first
  end

  def collection_title
    self[:collection_titles_sms]&.first
  end

  def collection_id
    self[:collection_record_id_sms]&.first
  end

  def serial_title
    self[:serial_titles_sms]&.first
  end

  def dc_right
    self['dc_right_display']
  end
  
  def dcterms_rights_holder
    self['dcterms_rights_holder_display']
  end

  def dcterms_provenance
    self['dcterms_provenance_display']
  end

  def dcterms_subject
    self['dcterms_subject_display']
  end

  def dlg_subject_personal
    self['dlg_subject_personal_display']
  end

  def dcterms_medium
    self['dcterms_medium_display']
  end

  def dcterms_creator
    self['dcterms_creator_display']
  end

  def dcterms_contributor
    self['dcterms_contributor_display']
  end

  def dcterms_publisher
    self['dcterms_publisher_display']
  end

  def dcterms_type
    self['dcterms_type_display']
  end

  def dcterms_format
    self['dcterms_format_display']
  end

  def dcterms_language
    self['dcterms_language_display']
  end

  def dcterms_description
    self['dcterms_description_display']
  end

  def dcterms_is_part_of
    self['dcterms_is_part_of_display']
  end

  def dc_date
    self['dc_date_display']
  end

  def dcterms_spatial
    self['dcterms_spatial_display']
  end

  def institution_slugs
    self['institution_slug_sms']
  end

  def serial_slug
    self['serial_slug_ss']
  end

  def created_at
    self['created_at_dts']
  end

  def updated_at
    self['updated_at_dts']
  end

  def external_identifiers
    return @external_identifiers unless @external_identifiers.nil?

    @external_identifiers = self[:external_identifiers_json_ss].nil? ? {} : JSON.parse(self[:external_identifiers_json_ss])
  rescue JSON::ParserError
    @external_identifiers = {}
  end

  def public_domain?
    PUBLIC_DOMAIN_DC_RIGHTS.include? self['dc_right_display'][0]
  end

  def downloadable?
    DOWNLOADABLE_DC_RIGHTS.include? dc_right&.first
  end
end
