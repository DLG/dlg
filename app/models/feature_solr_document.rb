# frozen_string_literal: true

# Represent a Bento from Solr
class FeatureSolrDocument < SolrDocument
  # @return [String] Title Type
  def title
    self[:title_ss]
  end

  # @return [Array<String>] Tile Link
  def title_link
    self[:title_link_ss]
  end

  # @return [Array<String>] Institution Name
  def institution_name
    self[:institution_ss]
  end

  # @return [Array<String>] Institution Link
  def institution_link
    self[:institution_link_ss]
  end

  # @return [String] Short Description
  def short_description
    self[:short_description_ss]
  end

  # @return [String] External Link
  def external_link
    self[:external_link_ss]
  end

  # @return [String] Image
  def image
    self[:image_ss]
  end

  # @return [String] Large Image
  def large_image
    self[:large_image_ss]
  end

  # @return [String] area
  def area
    self[:area_ss]
  end

  # @return [Boolean] primary_tab?
  def primary_tab?
    self[:primary_tab_bs]
  end

  # @return [Boolean] side_tab?
  def side_tab?
    self[:side_tab_bs]
  end
end
