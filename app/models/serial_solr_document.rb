# frozen_string_literal: true

class SerialSolrDocument < SolrDocument
  self.unique_key = 'slug_ss'

  def self.slug
    'slug_ss'
  end

  # @return [String]
  def slug
    self[:slug_ss]
  end

  # @return [String]
  def description_display
    self['dcterms_description_display']&.first
  end

  # @return [Array<String>]
  def dcterms_title
    self['dcterms_title_display']
  end

  # @return [Array<String>]
  def dcterms_identifier
    self['dcterms_subject_display']
  end

  # @return [Array<String>]
  def dcterms_publisher
    self['dcterms_publisher_display']
  end

  # @return [Array<String>]
  def dcterms_description
    self['dcterms_description_display']
  end

  # @return [Array<String>]
  def dcterms_subject
    self['dcterms_subject_display']
  end

  # @return [Array<String>]
  def dcterms_subject_fast
    self['dcterms_subject_fast_display']
  end

  # @return [Array<String>]
  def dcterms_medium
    self['dcterms_medium_display']
  end

  # @return [Array<String>]
  def dcterms_replaces
    self['dcterms_replaces_display']
  end

  # @return [Array<String>]
  def preceding_slugs
    self['preceding_slugs_sms']
  end

  # @return [Array<String>]
  def dcterms_is_replaced_by
    self['dcterms_is_replaced_by_display']
  end

  # @return [Array<String>]
  def succeeding_slugs
    self['succeeding_slugs_sms']
  end


  # @return [Array<String>]
  def dcterms_creator
    self['dcterms_creator_display']
  end

  # @return [Array<String>]
  def dcterms_subject_fast_display
    self['dcterms_subject_fast_display']
  end

  # @return [Array<String>]
  def dcterms_language_display
    self['dcterms_language_display']
  end

  # @return [Array<String>]
  def dcterms_frequency_display
    self['dcterms_frequency_display']
  end

  def thumbnail
    self[:thumbnail_ss]
  end

end
