# frozen_string_literal: true

# Represent an Event from Solr
class InstitutionSolrDocument < SolrDocument

  self.unique_key = 'slug_ss'

  # @return [String]
  def slug
    self[:slug_ss]
  end

  # @return [String] Title
  def title
    self[:title]
  end

  # @return [String] authoritative_name
  def authorized_name
    self[:authorized_name_ss]
  end  
  
  # @return [String] display_name
  def display_name
    self[:display_name_ss].present? ? self[:display_name_ss] : authorized_name
  end

  # @return [String] short_description
  def short_description
    self[:short_description_ss]
  end

  # @return [String] description
  def description
    self[:description_ss]
  end

  # @return [String] homepage_url
  def homepage_url
    self[:homepage_url_ss]
  end

  # @return [String] public_contact_address
  def public_contact_address
    self[:public_contact_address_ss]
  end

  # @return [String] public_contact_email
  def public_contact_email
    self[:public_contact_email_ss]
  end

  # @return [String] public_contact_phone
  def public_contact_phone
    self[:public_contact_phone_ss]
  end

  # @return [String] coordinates
  def coordinates
    self[:coordinates_ss]
  end

  # @return [String] parent_institution
  def parent_institution
    self[:parent_institution_ss]
  end

  # @return [String] strengths
  def strengths
    self[:strengths_ss]
  end

  # @return [String] first_char
  def first_char
    self[:first_char_ss]
  end

  # @return [Array<String>] image
  def image
    self[:image_ss]
  end

  # @return [String] bio_history
  def bio_history
    self[:bio_history_ss]&.html_safe
  end

  # @return [Array<String>] public_collections
  def public_collections
    return [] unless self[:public_collections_sms].present?

    collections = self[:public_collections_sms].map do |collection|
      JSON.parse(collection)
    end
    
    collections.filter { |collection| collection['portals']&.include?('georgia') }
  end

  def google_map_url
    "https://www.google.com/maps/search/?api=1&query=#{URI.encode_www_form_component(display_name)}"
  end

  def as_json(_)
    {
      institution_slug: slug,
      display_name: display_name,
      description: description,
      homepage_url: bio_history,
      strengths: strengths,
      parent_institution: parent_institution,
      public_contact_address: public_contact_address,
      public_contact_email: public_contact_email,
      public_contact_phone: public_contact_phone,
      coordinates: coordinates,
      public_collections: public_collections
    }
  end
end
