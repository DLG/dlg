# frozen_string_literal: true

class SerialsSearchBuilder < SearchBuilder
  def show_only_desired_classes(solr_parameters)
    solr_parameters[:fq] ||= []
    solr_parameters[:fq] << 'class_name_ss:Serial'
  end

  def self.filters
    base_filters + ['class_name_ss:Serial']
  end
end
