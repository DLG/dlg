# frozen_string_literal: true

# define search processor chain for all record search
class RecordSearchBuilder < SearchBuilder
  include BlacklightAdvancedSearch::AdvancedSearchBuilder
  self.default_processor_chain += %i[
    add_advanced_parse_q_to_solr
    add_advanced_search_to_solr
    limit_by_collection
    limit_by_institution
    limit_by_serial
    apply_api_v2_behavior
  ]

  def limit_by_collection(solr_parameters)
    return unless collection_specified

    solr_parameters[:fq] << "collection_record_id_sms:#{collection_specified}"
    solr_parameters['facet.field'].delete('collection_record_id_sms')
  end

  def limit_by_institution(solr_parameters)
    return unless institution_specified

    solr_parameters[:fq] << "institution_slug_sms:#{institution_specified}"
    solr_parameters['facet.field'].delete('institution_slug_sms')
  end

  def limit_by_serial(solr_parameters)
    return unless serial_specified

    solr_parameters[:fq] << "serial_slug_ss:#{serial_specified}"
    solr_parameters['facet.field'].delete('serial_slug_ss')
  end

  def show_only_desired_classes(solr_parameters)
    solr_parameters[:fq] ||= []
    if FeatureFlags.enabled? :serials
      solr_parameters[:fq] << 'class_name_ss:(Item OR Collection OR Serial)'
    else
      solr_parameters[:fq] << 'class_name_ss:(Item OR Collection)'
    end
  end

  def self.item_filters
    base_filters + ['class_name_ss:Item']
  end

  def self.collection_filters
    base_filters + ['class_name_ss:Collection']
  end

  def self.record_filters
    if FeatureFlags.enabled? :serials
      base_filters + ['class_name_ss:(Item OR Collection OR Serial)']
    else
      base_filters + ['class_name_ss:(Item OR Collection)']
    end
  end

  private

  def collection_specified
    blacklight_params[:collection_record_id]
  end

  def institution_specified
    blacklight_params[:institution_slug]
  end

  def serial_specified
    blacklight_params[:serial_record_id]
  end

  def apply_api_v2_behavior(solr_parameters)
    if %w[json xml].include?(blacklight_params[:format]) && blacklight_params[:api_version] == '2'
      solr_parameters['facet.field']&.delete 'geojson'
      solr_parameters.delete 'f.geojson.facet.limit'
      fields = RecordsController.blacklight_config.show_fields.keys
      fields |= RecordsController.blacklight_config.index_fields.keys
      fields |= SolrDocument.field_semantics.values
      fields |= %w[id collection_record_id_sms title]
      fields |= ['fulltext_texts:if(termfreq(fulltext_present_ss,"Yes"),true,false)']
      solr_parameters['fl'] = fields
    end
  end
end
