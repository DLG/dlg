import * as Leaflet from 'leaflet/dist/leaflet-src.esm'
import 'leaflet/dist/leaflet.css'
function initialize(){
    $('[data-coordinates]').each((i, el) => {
        const coordinates = $(el).data('coordinates').split(/, */).map(parseFloat);
        const instName = $(el).data('inst-name');
        const googleLink = $(el).data('google-link');
        const linkElem =  $('<a>',{
            text: instName,
            href: googleLink
        });
        const map = Leaflet.map(el, {
            scrollWheelZoom: false,
            center: coordinates,
            zoom: 11
        });
        Leaflet.tileLayer(
            'https://{s}.basemaps.cartocdn.com/light_nolabels/{z}/{x}/{y}.png',
            {
                id: 'osm.default',
                attribution: '© OpenStreetMap, © CartoDB'
            }
        ).addTo(map);
        delete Leaflet.Icon.Default.prototype._getIconUrl;
        Leaflet.Icon.Default.mergeOptions({
            iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
            iconUrl: require('leaflet/dist/images/marker-icon.png'),
            shadowUrl: require('leaflet/dist/images/marker-shadow.png'),
        });
        Leaflet.marker(coordinates)
            .addTo(map)
            .bindPopup(linkElem.prop('outerHTML'))
            .openPopup();
    });
}

Blacklight.onLoad(initialize);
initialize();


