import { Component } from 'react';
import * as actions from 'mirador/dist/es/src/state/actions';
import { getViewer, getSequence, getCurrentCanvas } from 'mirador/dist/es/src/state/selectors';
import { OSDReferences } from 'mirador/dist/es/src/plugins/OSDReferences';

function sendToQueryString(params) {
    const searchParams = (new URL(window.location)).searchParams;
    for (let key in params) {
        if (params.hasOwnProperty(key)) {
            let value = params[key];
            searchParams.set(key, parseInt(value));
        }
    }
    const qs = '?' + searchParams.toString();
    history.replaceState(history.state, 'arbitrary title', qs);
}

function parseQueryString() {
    const searchParams = (new URL(window.location)).searchParams;
    let params = {};
    searchParams.forEach((value, key) => {
        params[key] = value && parseInt(value)
    });
    return params;
}

class QueryStringComponent extends Component {
    timesInitialStatePushed = 0;
    initialViewState = null;
    initialCanvasId = null;
    canvasSuccessfullySet = false;
    leftPageViaTurbolinks = false;

    render() {
        return null;
    }

    sendToQueryStringIfStillActive(params) {
        if (!this.leftPageViaTurbolinks) sendToQueryString(params);
    }

    restoreFromQueryString() {
        if (this.props.perWindowAttributes['initialized']) return;

        let qsProps = parseQueryString();
        if (!(qsProps.hasOwnProperty('x') && qsProps.hasOwnProperty('y') && qsProps.hasOwnProperty('w'))) {
            return;
        }
        let viewProps = {
            x: qsProps.x,
            y: qsProps.y,
            zoom: 1/qsProps.w
        }
        viewProps.flip = !!qsProps.flip;
        viewProps.rotation = qsProps.rotate || 0;
        this.initialViewState = viewProps;
        this.initialCanvasId = qsProps.canvas;
        this.setCanvasFromIndex(qsProps.canvas)
        this.props.updateViewport(this.props.windowId, this.initialViewState);
        this.props.perWindowAttributes['initialized'] = true;
    }

    setCanvasFromIndex(canvasIndex) {
        if (this.canvasSuccessfullySet) return;
        const sequence = this.props.sequence;
        const canvas = sequence && sequence.items && sequence.items[canvasIndex];
        const canvasId = canvas && canvas.id;
        if (canvasId) {
            this.props.setCanvas(this.props.windowId, canvasId);
            this.canvasSuccessfullySet = true;
        }
    }

    pushInitialState() {
        if (this.initialViewState && this.timesInitialStatePushed++ < 3) {
            this.setCanvasFromIndex(this.initialCanvasId);
            this.props.updateViewport(this.props.windowId, this.initialViewState);
        }
    }

    componentDidMount() {
        this.restoreFromQueryString();
        this.pushInitialState();

        const self = this;
        let turbolinksNavigateHandler = function () {
            self.leftPageViaTurbolinks = true;
            document.removeEventListener('turbolinks:before-visit', turbolinksNavigateHandler);
        }
        document.addEventListener('turbolinks:before-visit', turbolinksNavigateHandler);
    }

    componentDidUpdate() {
        this.pushInitialState();
        const canvasIndex = this.props.canvas && this.props.canvas.index;
        const viewer = this.props.viewer;
        if (canvasIndex != undefined && viewer) {
            let params = {
                canvas: canvasIndex,
                x: viewer.x,
                y: viewer.y,
                w: 1 / viewer.zoom
            };
            if (viewer.rotation) params.rotate = viewer.rotation;
            if (viewer.flip) params.flip = 1;
            this.sendToQueryStringIfStillActive(params);
        }
    }
}

function mapStateToProps(state, { windowId }) {
    QueryStringComponent.windowCache ||= {};
    QueryStringComponent.windowCache[windowId] ||= {};
    const osd = OSDReferences.get(windowId);
    const sequence = getSequence(state, {windowId});
    const canvas = getCurrentCanvas(state, {sequence, windowId});
    return {
        manifests: state.manifests,
        viewport: osd && osd.current && osd.current.viewport,
        viewer: getViewer(state, {windowId}),
        perWindowAttributes: QueryStringComponent.windowCache[windowId],
        sequence,
        canvas
    };
}

const mapDispatchToProps = {
    updateWindow: actions.updateWindow,
    updateViewport: actions.updateViewport,
    setCanvas: actions.setCanvas
};

export default {
    target: 'OpenSeadragonViewer',
    mode: 'add',
    name: 'DLGQueryStringPlugin',
    component: QueryStringComponent,
    mapStateToProps,
    mapDispatchToProps,
};
