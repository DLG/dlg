// This is our modified copy of https://github.com/ProjectMirador/mirador-dl-plugin
// It's a temporary solution until we can make a PR on the parent project or set up a proper fork.
import miradorDownloadPlugin from './miradorDownloadPlugin';
import MiradorDownloadDialogPlugin from './MiradorDownloadDialog';

export {
  miradorDownloadPlugin,
  MiradorDownloadDialogPlugin,
};

export default [
  miradorDownloadPlugin,
  MiradorDownloadDialogPlugin,
];
