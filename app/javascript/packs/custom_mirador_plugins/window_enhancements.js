import { Component } from 'react';
import PropTypes from 'prop-types'
import ActionTypes from "mirador/dist/es/src/state/actions/action-types";
import { OSDReferences } from 'mirador/dist/es/src/plugins/OSDReferences';

function forceOSDViewportToRecomputeHeight(osdViewport) {
    const size = osdViewport.containerSize;
    size.y = osdViewport.viewer.element.offsetHeight;
    osdViewport.resize(size);
}

const statuses = {
    AWAITING_MANIFEST: 1,
    AWAITING_UI_RESIZE: 2,
    DONE: 3
}

class WindowEnhancementsComponent extends Component {
    status = statuses.AWAITING_MANIFEST;
    previousCanvasId = null;

    render() {
        return null;
    }

    componentDidMount() {
        this.checkIfShouldShowThumbnails();
    }

    componentDidUpdate() {
        this.checkIfShouldShowThumbnails();
        this.checkIfCanvasChanged();
    }

    checkIfCanvasChanged() {
        if (this.props.canvasId !== this.previousCanvasId) {
            this.canvasChanged();
            this.previousCanvasId = this.props.canvasId;
        }
    }

    canvasChanged() {
        let manifest = this.manifest();
        let items = manifest && manifest.items;
        if (!items) return;
        let canvas = items.find(x => x.id === this.props.canvasId)
        if (!canvas) return;
        if (canvas.duration) {
            this.afterChangeToAudioVideoCanvas(canvas);
        } else {
            this.afterChangeToNonAudioVideoCanvas(canvas);
        }
    }

    afterChangeToAudioVideoCanvas(canvas) {
        setTimeout(() => this.setSourceDirectlyOnVideoElement());
    }

    afterChangeToNonAudioVideoCanvas(canvas) {
        this.showPluginMenuIfHidden();
    }

    setSourceDirectlyOnVideoElement() {
        this.miradorElement().querySelectorAll('video').forEach(x => x.src = x.lastChild.src);
        this.hidePluginMenuIfOnlyItemIsImageTools();
    }

    hidePluginMenuIfOnlyItemIsImageTools() {
        const pluginMenuPlugins = (this.miradorElement()?.mirador?.plugins || [])
            .filter(x => x.target === 'WindowTopBarPluginMenu');
        if (pluginMenuPlugins.length === 1 && pluginMenuPlugins[0].component?.name === 'MiradorImageToolsMenuItem') {
            $(this.miradorElement())
                .find('nav[aria-label="Window navigation"] button[aria-label="Window options"]')
                .hide();
        }
    }

    showPluginMenuIfHidden() {
        $(this.miradorElement())
            .find('nav[aria-label="Window navigation"] button[aria-label="Window options"]')
            .show();
    }

    manifest() {
        let manifest = this.props.manifests[this.props.window.manifestId];
        return manifest && manifest.json;
    }

    miradorElement() {
        return document.getElementById(this.props.miradorElementId);
    }

    checkIfShouldShowThumbnails() {
        if (this.status === statuses.DONE) return;
        const miradorWindow = this.props.window;
        const viewport = this.props.viewport;
        let manifest = this.manifest();
        if (this.status === statuses.AWAITING_MANIFEST && manifest) {
            let showThumbnails = false;
            // sequences = old format; items = new format
            let sequences = manifest.sequences || manifest.items;
            if (!sequences || sequences.length > 1 || sequences.length === 0) {
                // Show thumbnails if there are multiple items...
                // also if there are no or undefined sequences (an unexpected condition so do the more flexible thing)
                showThumbnails = true;
            } else {
                // if there's exactly one item, see if it has multiple canvases
                let canvases = sequences[0].canvases;
                if (canvases && canvases.length > 1) {
                    showThumbnails = true;
                }
            }
            if (showThumbnails) {
                this.props.updateThumbnailNav(miradorWindow.thumbnailNavigationId, 'far-bottom');
                this.status = statuses.AWAITING_UI_RESIZE;
            } else {
                this.props.updateThumbnailNav(miradorWindow.thumbnailNavigationId, 'none');
                this.status = statuses.DONE;
            }
        } else if (this.status === statuses.AWAITING_UI_RESIZE && viewport) {
            forceOSDViewportToRecomputeHeight(viewport);
            this.status = statuses.DONE;
        }
    }

    showSideBar() {
        if (!this.props.sideBarOpen) {
            this.props.toggleSidePanel(this.props.windowId);
        }
    }

    hideSideBar() {
        if (this.props.sideBarOpen) {
            this.props.toggleSidePanel(this.props.windowId);
        }
    }

}

function mapStateToProps(state, { windowId }) {
    const osd = OSDReferences.get(windowId);
    return {
        manifests: state.manifests,
        viewport: osd && osd.current && osd.current.viewport,
        windowId,
        sideBarOpen: state.windows[windowId].sideBarOpen,
        canvasId: state.windows[windowId].canvasId,
        miradorElementId: state.config.id
    };
}

function mapDispatchToProps(dispatch) {
    return {
        updateThumbnailNav: (thumbnailNavigationId, position) => dispatch({
            id: thumbnailNavigationId,
            payload: { position },
            type: ActionTypes.UPDATE_COMPANION_WINDOW,
        }),
        toggleSidePanel: (windowId) => dispatch({
            type: ActionTypes.TOGGLE_WINDOW_SIDE_BAR,
            windowId
        })
    };
}

export default {
    target: 'Window',
    mode: 'add',
    name: 'DLGWindowEnhancementsPlugin',
    component: WindowEnhancementsComponent,
    mapStateToProps,
    mapDispatchToProps,
};