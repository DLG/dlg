function setupAdvancedSearch() {
    $('select.advanced-search-facet-select:visible').chosen({
        search_contains: true,
        width: '100%'
    });
    $('select.advanced-search-facet-select[data-suggest]').each(function (_i, selectElement) {
        const cachedSuggestions = {};
        const chosenData = $(selectElement).data('chosen');
        const suggestionService = $(selectElement).data('suggest');
        const $searchBox = chosenData.search_field;
        const chosenResultsData = chosenData.results_data;
        const searchBoxBlank = () => $searchBox.val().trim() && $searchBox.val() !== chosenData.default_text;
        const updateDropdown = () => {
            if ($searchBox.is(':focus') && !searchBoxBlank()) {
                $searchBox.trigger('keyup');
            }
        };
        $searchBox.on('input', function () {
           const text = this.value;
           $.get(suggestionService, { q: text }).then(function (suggestions) {
               let anySuggestionsAdded = false;
               suggestions.forEach( suggestion => {
                   if (cachedSuggestions[suggestion.value] === undefined) {
                       cachedSuggestions[suggestion.value] = suggestion.text;
                       $(`<option value="${suggestion.value}">${suggestion.text}</option>`).appendTo(selectElement);
                       chosenResultsData.push({
                           array_index: chosenResultsData.length,
                           options_index: chosenResultsData.length,
                           value: suggestion.value,
                           text: suggestion.text,
                           html: suggestion.text.replaceAll(/  +/g, x => x.replaceAll(' ', '&nbsp;'))
                       });
                       anySuggestionsAdded = true;
                   }
               });
               if (anySuggestionsAdded) updateDropdown();
           });
        });
    });
}

Blacklight.onLoad(setupAdvancedSearch);
setupAdvancedSearch();