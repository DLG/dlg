/* eslint no-console:0 */
// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.
//
// To reference this file, add <%= javascript_pack_tag 'application' %> to the appropriate
// layout file, like app/views/layouts/application.html.erb


// Uncomment to copy all static images under ../images to the output folder and reference
// them with the image_pack_tag helper in views (e.g <%= image_pack_tag 'rails.png' %>)
// or the `imagePath` JavaScript helper below.
//
// const images = require.context('../images', true)
// const imagePath = (name) => images(name, true)

import Mirador from 'mirador';
import miradorImageToolsPlugin from 'mirador-image-tools/es/plugins/miradorImageToolsPlugin.js'
import miradorDownloadPlugins from './custom_mirador_plugins/dlg-mirador-dl-plugin'
import windowEnhancementsPlugin from './custom_mirador_plugins/window_enhancements';
import queryStringPlugin from './custom_mirador_plugins/query_string_sync';

const commonConfig = {
    language: 'en', // The default language set in the application
    selectedTheme: 'light',
    window: {
        defaultSideBarPanel: 'info',
        sideBarOpenByDefault: true,
        //hideWindowTitle: true,
        sideBarOpen: false
    },
    themes: {
        light: {
            palette: {
                type: 'light',
                primary: {
                    main: '#0E8893',
                },
            },
        },
    },
    osdConfig: {
        viewportMargins: {
            bottom: 54
        }
    },
    videoOptions: {},
    workspace: {
        showZoomControls: true,
        type: 'mosaic',
        viewportPosition: { // center coordinates for the elastic mode workspace
            x: 0,
            y: 0,
        },
        draggingEnabled: true,
        allowNewWindows: true,
    },
    workspaceControlPanel: {
        enabled: false
    }
};

function embeddedConfig(element, manifest) {
    return $.extend(true, {}, commonConfig, {
        id: element.id,
        window: {
            imageToolsEnabled: true
        },
        windows: [{
            allowClose: false,
            loadedManifest: manifest,
            allowFullscreen: true,
            allowMaximize: false,
            sideBarOpen: false
        }],
        workspaceControlPanel: {
            enabled: false
        }
    });
}

function fullFeaturedEmbeddedConfig(element, manifest) {
    return $.extend(true, {}, commonConfig, {
        id: element.id,
        window: {
            imageToolsEnabled: true
        },
        windows: [{
            allowClose: false,
            loadedManifest: manifest,
        }],
        workspaceControlPanel: {
            enabled: true
        }
    });
}

function dedicatedPageConfig(element, manifest) {
    return $.extend(true, {}, commonConfig, {
        id: element.id,
        window: {
            imageToolsEnabled: true
        },
        windows: [{
            loadedManifest: manifest,
        }],
        workspaceControlPanel: {
            enabled: true
        }
    });
}

function getConfigBuilder(minimalMiradorSetting, viewerDataMirador) {
    if (viewerDataMirador === 'full') return dedicatedPageConfig;
    if (viewerDataMirador === 'basic' || minimalMiradorSetting) return embeddedConfig;
    return fullFeaturedEmbeddedConfig;
}

$(function () {

        var minimalMirador = !!parseInt(localStorage['minimalMirador']) || (localStorage['minimalMirador'] == undefined);
        var checkbox = $('[data-full-mirador-toggle]')[0];
        if (checkbox) {
            checkbox.checked = !minimalMirador;
            $(checkbox).on('change', function (){
                localStorage['minimalMirador'] = Number(!checkbox.checked);
                location.reload();
            });
        }

        $('[data-mirador]').each(function (i, element) {
            ensureElementHasId(element);
            const manifest = $(element).data('presentation-manifest');
            const thumbnailPosition = $(element).data('thumbnail-position');
            const itemDownloadable = element.hasAttribute('data-downloadable');
            const configBuilder = getConfigBuilder(minimalMirador, $(element).data('mirador'));
            const config = configBuilder(element, manifest);
            config.thumbnailNavigation = {
                defaultPosition: thumbnailPosition,
            }
            const plugins = [
                miradorImageToolsPlugin,
                windowEnhancementsPlugin,
                queryStringPlugin
            ];
            if (itemDownloadable) {
                plugins.push(...miradorDownloadPlugins)
            } else {
                config['videoOptions']['controlsList'] = 'nodownload';
                $(element).on('contextmenu', 'canvas', function () { return false; });
            }
            element.mirador = Mirador.viewer(config, plugins);
            // set up "Embed" modal
            let embedTextarea = $('[data-embed-html]')
            if (embedTextarea.length) {
                function updateIframeHtml() {
                    let width = parseInt($('[data-embed-size-selector]')[0].value);
                    embedTextarea.html(`<iframe src="${location.origin}/iiif-embeddable/?manifest=${manifest}" title="Document viewer" width="${width}" height="${width * 3 / 4}" allowfullscreen frameborder="0"></iframe>`);
                }

                $('[data-embed-size-selector]').on('change', updateIframeHtml);
                updateIframeHtml();
            }
        });
    }
);

function ensureElementHasId(element) {
    if (!element.id) element.id = 'mirador-' + Math.floor(Math.random() * 100000);
}
