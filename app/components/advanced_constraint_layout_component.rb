# frozen_string_literal: true

class AdvancedConstraintLayoutComponent < Blacklight::ConstraintLayoutComponent

  def initialize(opts = {})
    if opts[:value]
      split_value = opts[:value].split(/\bOR\b/)
      if opts[:value].html_safe?
        split_value = split_value.map &:html_safe
      end
      opts[:value] = safe_join split_value,
                               '<strong class="text-muted constraint-connector">OR</strong>'.html_safe
    end
    super(**opts)
  end

end