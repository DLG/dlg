# frozen_string_literal: true

class DocumentTitleComponentWithBadges < Blacklight::DocumentTitleComponent
  def title
    document_link = if presenter.document.klass == 'Collection'
                      helpers.collection_index_link_to presenter.document
                    elsif presenter.document.klass == 'Serial'
                      helpers.serial_index_link_to presenter.document
                    else
                      super
                    end
    # don't use "#{}" because it won't preserve HTML-safe status
    "".html_safe + collection_icon + serial_icon + fulltext_icon + document_link
  end

  def collection_icon
    return unless @document.collection?
    "<span class='badge badge-primary'><i class='fa fa-asterisk'></i> #{ I18n.t('search.index.collection_label') }</span> ".html_safe
  end

  def serial_icon
    return unless @document.serial?
    "<span class='badge badge-primary'><i class='fa fa-asterisk'></i> #{ I18n.t('search.index.serial_label') }</span> ".html_safe
  end

  def fulltext_icon
    return unless @document.item? && @document.fulltext
    "<span class='badge badge-info'><i class='fa fa-asterisk'></i> #{ I18n.t('search.index.has_fulltext') }</span> ".html_safe
  end

end