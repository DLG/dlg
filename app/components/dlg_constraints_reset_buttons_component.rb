class DlgConstraintsResetButtonsComponent < ViewComponent::Base
  def initialize(start_over_component: Blacklight::StartOverButtonComponent)
    @start_over_component = start_over_component
  end

  def collection
    controller.instance_variable_get(:@collection)
  end
end
