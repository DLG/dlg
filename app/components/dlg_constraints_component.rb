# frozen_string_literal: true

class DlgConstraintsComponent < Blacklight::ConstraintsComponent

  def initialize(opts = {})
    opts[:facet_constraint_component_options] = (opts[:facet_constraint_component_options]|| {}).merge({
        layout: AdvancedConstraintLayoutComponent
      })
    opts[:start_over_component] = DlgConstraintsResetButtonsComponent
    super(**opts)
  end

  def render?
    super || @search_state.params.include?('range')
  end

  def query_constraints
    super + advanced_keyword_constraints_area(controller.try :advanced_query)
  end

  def advanced_keyword_constraints_area(advanced_query)
    items = advanced_keyword_constraints_items(advanced_query)
    if advanced_query&.keyword_op == 'OR' && advanced_query.keyword_queries.length > 1
      content_tag :span, class: 'inclusive_or appliedFilter' do
        content_tag(:span, 'Any of:', class: 'operator') + items
      end
    else
      items
    end
  end

  def advanced_keyword_constraints_items(advanced_query)
    safe_join((advanced_query&.keyword_queries || []).map {|field, value|
      label = helpers.blacklight_config.search_fields[field][:label]
      helpers.render(
        @query_constraint_component.new(
          search_state: @search_state,
          value: value,
          label: label,
          remove_path: view_context.search_action_path(@search_state.reset_search({ field => nil }).to_h),
          classes: 'query',
          **@query_constraint_component_options
        )
      )
    })
  end

  def range_constraints_items

  end

end