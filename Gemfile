# frozen_string_literal: true

source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?('/')
  "https://github.com/#{repo_name}.git"
end

ruby '3.2.2'

gem 'active_hash'
gem 'addressable', '~>2.8.0'
gem 'blacklight', '7.30'
gem 'blacklight_advanced_search', '7.0'
gem 'blacklight-gallery'
gem 'blacklight-maps', github: 'GIL-GALILEO/blacklight-maps'
gem 'blacklight_range_limit', '~> 8.0'
gem 'bootstrap', '~> 4.0'
gem 'chosen-rails', '~> 1.10.0'
gem 'coffee-rails', '~> 5.0.0'
gem 'exception_notification'
gem 'httparty', '~> 0.21.0'
gem 'jbuilder', '~> 2.11'
gem 'jquery-rails'
gem 'leaflet-rails', '= 1.0.3'
gem 'openseadragon'
gem 'pg'
gem 'psych', '~>3.0'
gem 'puma'
gem 'rails', '~> 7.0'
gem 'rsolr', '>= 1.0'
gem 'sass-rails', '~> 6.0'
gem "sentry-ruby"
gem "sentry-rails"
gem 'sitemap_generator'
gem 'slack-notifier'
gem "sprockets-rails"
gem 'turbolinks', '~> 5'
gem 'webpacker', '~> 4.0'
gem 'recaptcha'

group :development, :test do
  gem 'capybara'
  gem 'capybara-screenshot'
  gem 'cuprite'
  gem 'rspec-rails', '5.1'
  gem 'webmock'
end

group :development do
  gem 'letter_opener'
  gem 'listen', '~> 3.2'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'web-console'
end

gem "font-awesome-rails", "~> 4.7"
