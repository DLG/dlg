# Changelog
All notable changes to the DLG public site project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## [20250212] (MidSprint D25)
### Changed
- Remove carousel and add hero image (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/598)
- Changes to hero at small sizes (https://gitlab.galileo.usg.edu/DLG/dlg/-/merge_requests/521)
- Turn hero text into div (https://gitlab.galileo.usg.edu/DLG/dlg/-/merge_requests/522)
- The heading structure for the site is not well-formed (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/556)


## [20250124] (Sprint B25)
### Changed
- Rails 7 Upgrade (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/548)
- Fix gallery view header white space (https://gitlab.galileo.usg.edu/DLG/dlg/-/merge_requests/513)

## [20250115] (Sprint A25)
### Changed
- Remove twitter/X links (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/595)
- There is no method to skip to the main content included in the website (Skip Link) (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/565)
- The gray color used for some text throughout the website does not meet WCAG minimums for color contrast (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/560)
- The light red color used for focus indicator of the individual accordion sections included on the Institutions page does not meet WCAG minimums for color contrast (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/563)
- The teal color used for some text throughout the website does not meet WCAG minimums for color contrast (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/559)
- Page title could be improved (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/586)
- Focus order should be improved (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/594)
- Counties: Links could be improved (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/587)
- Grid/List View links could be improved (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/585)
- Alternative text for linked images could be improved (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/591)
- Link text could be improved (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/588)
- Heading markup for Searching Help modal could be improved (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/593)
- There article cards included on the Collection page are using <header> tag inappropriately (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/568)
- Required fields should be indicated in label (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/592)
- The Match type select element included on the Search page does not have an accessible name (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/579)
- Decorative images in the Hero section are not marked up as such (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/590)
- OCR: Image is missing an alt attribute (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/589)
- The alternative text for the Rights statement image included on the Collection page with text does not provide equivalent information (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/551)
- An arialabelledby attribute has been included on the <div> tags containing the expanded information for the institutions accordion widget on the Institutions page (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/574)
- The page indicators in the pagination widget for the Collections page are not using aria-lables appropriately (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/569)


## [20241213] (Sprint Y24)
### Changed
- The Published and Unpublished radio buttons included in the Nominate Collections form do not have accessible names (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/575)
- "All items" and “Read More” are not very descriptive. (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/580)
- Update ambiguous links on institution index page (https://gitlab.galileo.usg.edu/DLG/dlg/-/merge_requests/474)
- Headings are <h3> should be <h2>. (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/582)
- Social media icons, search help button, and search button are not labeled. (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/581)
- Add main tag to homepage layout (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/583)
- The potentially harmful content modal included on the homepage does not have an accessible name (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/572)
- The SVGs included in the contact links in the website's header do not have image roles (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/571)
- The Search input field included on the Search page is missing an autocomplete attribute (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/557)

## [20241101] (Sprint V24)
### Changed
- Update Rails (https://gitlab.galileo.usg.edu/DLG/dlg/-/merge_requests/468)

## [20241018] (Sprint U24)
### Changed
- Minor rails update 

## [20240517] (Sprint J24)
### Changed
- Turn off auto play of features (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/531)
- DLG and me alt image text bug (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/532)

## [20240209] (Sprint C24)
### Changed
- Error handling improvements (https://gitlab.galileo.usg.edu/DLG/dlg/-/merge_requests/447)
- Add development location of dlg_av to .gitignore (https://gitlab.galileo.usg.edu/DLG/dlg/-/merge_requests/448)
- Fix full-size download issue in Mirador (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/519)
- Change "download suffix" for Mirador download plugin (https://gitlab.galileo.usg.edu/DLG/dlg/-/merge_requests/450)
- Lighthouse suggestions (https://gitlab.galileo.usg.edu/DLG/dlg/-/merge_requests/451)

## [20240126] (Sprint B24)
### Changed
- Partners and Sponsors page update (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/521)
- Turn Serials on in Prod (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/523)

## [20231020] (Sprint U23)
### Changed
- Change Twitter logo to X (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/513)
- Preceding Call number search fixes (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/514)

## [20230922] (Sprint S23)
### Changed
- Make all links in the short collection description clickable (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/510)
- Call number search under feature flag (https://gitlab.galileo.usg.edu/DLG/dlg/-/merge_requests/429)

## [20230908] (Sprint R23)
### Fixed
- Mirador: selecting page from gallery view does not work (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/507)

## [20230825] (Sprint Q23)
### Changed
- Tweak Solr search field weights and other search behaviors (https://gitlab.galileo.usg.edu/DLG/dlg/-/merge_requests/421)
- Create temporary redirects for serials to legacy GGP site (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/505)
- Update rails and puma (https://gitlab.galileo.usg.edu/DLG/dlg/-/merge_requests/420)

### Staging
- GGP call number search (https://gitlab.galileo.usg.edu/DLG/dlg/-/merge_requests/416, https://gitlab.galileo.usg.edu/DLG/dlg/-/merge_requests/417, https://gitlab.galileo.usg.edu/DLG/dlg/-/merge_requests/418)

## [20230811] (Sprint P23)

### Fixed
- Nonexistent collections should be a 404 error (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/474)

### Staging
- Remove serial description (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/499)


## [20230630] (Sprint M23)
### Changed
- Add GTM code to site (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/484)
- Update Rails to 6.1.7.4 (https://gitlab.galileo.usg.edu/DLG/dlg/-/merge_requests/404/diffs?commit_id=f181c65680b81b1cb9f0f89ede176cd93b392abc)

### Staging
- Serials: location filters (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/495)
- Serials: show links to preceding and succeeding titles (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/496)

## [20230602] (Sprint K23)
### Staging
- Add serials to record page (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/489)
- Serial images (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/492)

## [20230519] (Sprint J23)
### Fixed
- Make year range form stop putting absolute URL in action (https://gitlab.galileo.usg.edu/DLG/dlg/-/merge_requests/397)
- Fix links in homepage "Newspapers" tab (https://gitlab.galileo.usg.edu/DLG/dlg/-/merge_requests/398)

### Staging
- Add year filter to serials (https://gitlab.galileo.usg.edu/DLG/dlg/-/merge_requests/396)


## [20230505] (Sprint I23)
### Staging
- Create Serials route and index (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/488)
- Add Serials filter (https://gitlab.galileo.usg.edu/DLG/dlg/-/merge_requests/394)

## [20230113] (Sprint A23)
### Changed
- Update connect menu link (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/466)
- Update HTTParty (https://gitlab.galileo.usg.edu/DLG/dlg/-/merge_requests/290/diffs?commit_id=1cb427b971a4b466db54246090eb549fa94a762e)

## [20221202] (Sprint X22)
### Changed
- Add Sentry to dlg (https://gitlab.galileo.usg.edu/DLG/dlg/-/merge_requests/281)
- Turn on sentry in production (https://gitlab.galileo.usg.edu/DLG/dlg/-/merge_requests/283)

### Fixed
- Try to auto-fire up the webserver on CI/CD deploys (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/462)

## [20220928] (Mid-Sprint T22)
### Fixed
- Fix performance issue in DLG API version for bento (https://gitlab.galileo.usg.edu/DLG/dlg/-/merge_requests/276)

## [20220908] (Sprint R22)
### Changed
- Add secondary title_sort to date sort (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/450)

## [20220829] (Sprint Q22)
### Changed
- Custom page url not working in Firefox (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/440)
- Conditionally disable right-click in Mirador (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/441)
- Date sort doesn't sort by month and day (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/392)
- Create API version that doesn't include GeoJSON facet or Full Text (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/444)
- Fix sitemap generation (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/447)
- Fix link to Featured Collections on Explore dropdown (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/448)
- Change wording for date sort (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/446)


## [20220812] (Sprint P22)
### Added
- create new contact form (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/433)

### Changed
- Contact form revisited (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/439)
- update robots.txt to disable crawling for staging (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/442)


## [20220721] (Sprint N22)
### Changed
- Update Rails to 5.2.8.1 (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/435)
- Update Blacklight to 6.25 (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/604)

## [20220624] (Mid-Sprint M22)
### Changed
- Improvements to Mirador and social media meta tags — backport from CRDL (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/428)
- Remove SMS Record option (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/430)
- Update footer (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/429)


## [20220621] (Sprint L22)
### Changed
- Add advanced search tips to the main search bar tool tip (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/424)

## [20220603] (Sprint K22)

### Changed
- Update rack and nokogiri (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/426)

## [20220506] (Sprint I22)

### Changed
- Upgrade rails (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/423)

## [20220422] (Sprint H22)

### Changed
- Upgrade nokogiri and puma (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/421)

### Fixed
- Fix results so collection displays (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/420)

## [20220325] (Sprint F22)

### Changed
- Make improvements to Carousel (match CRDL) (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/413)
- Fix facebook social icon border radius to match other icons (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/414)
- Updating broken links on DLG contribute page (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/412)

### Added
- Add iiif_manifest to public json (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/391)
- add feature flag to carousel (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/416)

## [20220225] (Sprint D22)
- Update Rails (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/404)
- Update link to Promotional materials on dropdowns (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/398)
- Update DLG and Me (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/405)

## [20220211] (Sprint C22)
- Update ruby to 2.6.9 (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/395)
- Change staging log level to warn (https://gitlab.galileo.usg.edu/DLG/dlg/-/merge_requests/228)
- Add Maintenance Banner (https://gitlab.galileo.usg.edu/DLG/dlg/-/merge_requests/229)

## [20220128] (Sprint B22)

### Changed
- Filter out solr Name records (https://gitlab.galileo.usg.edu/DLG/dlg/-/merge_requests/225)
- Update README (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/399)

## [20220114] (Sprint A22)

### Changed
- Fix harmful content cookie to only set on homepage (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/396) 

## [20220107] (Mid Sprint A22)

### Added
- Create a GALILEO comment form for harmful content. (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/374)

### Changed
- Remove "harmful content" from menu (https://gitlab.galileo.usg.edu/DLG/dlg/-/merge_requests/212)
- Please create harmful content notification when users land on home page (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/390)
- Replaced CONST with VAR (https://gitlab.galileo.usg.edu/DLG/dlg/-/merge_requests/216)
- Updates to harmful content text (https://gitlab.galileo.usg.edu/DLG/dlg/-/merge_requests/218)
- Move harmful content to about controller (https://gitlab.galileo.usg.edu/DLG/dlg/-/merge_requests/219)
- update harnmful form text (https://gitlab.galileo.usg.edu/DLG/dlg/-/merge_requests/220)

### Added
- Fix main search layout issue in mobile (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/383)
 - Please replace current family history logo with new one (in footer) (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/384)

## [2021021] (Sprint U21)

### Added
- Fix main search layout issue in mobile (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/383)
 - Please replace current family history logo with new one (in footer) (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/384)

## [2021007] (Sprint T21 Hotfix)

### Added
 - Remove some Mirador-specific buttons from non-Mirador record pages (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/382)

## [2021007] (Sprint T21)

### Added
 - Turn Mirador On! (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/380) 
 - Try to increase size of zoom etc icons (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/377)
 - Promote download button to Mirador window toolbar (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/378)
 - optimize js package size (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/351)

## [20210927] (Mid-Sprint T21)

### Changed
  - Update text on share menu v2 (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/368)
  - How to link to supplementary materials that aren’t hosted within DLG? (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/371)

### Staging
 - Mirador viewer: change the More about this viewer link (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/370)


## [20210826] (Sprint Q21)

### Changed
 - Update text on share menu https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/365

### Fixed
 - Full text and item viewer no longer displaying in tabs on dlg.usg.edu https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/364

### Staging
- Data issue in staging, thumbnail links in results are linking to production https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/366
- Narrow the CSS selector targeting the misaligned divider for viewport zoom controls https://gitlab.galileo.usg.edu/DLG/dlg/-/merge_requests/184
- Add og:url meta tag https://gitlab.galileo.usg.edu/DLG/dlg/-/merge_requests/185

## [20210812] (Sprint P21)

### Changed
- Update DLG and Me section of website https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/356

### Staging
- Sync Mirador page number and crop coordinates to query string https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/343
- Social media meta tags https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/345
- Add `rel="noopener"` or `rel="noreferrer"` to any external links to improve performance and prevent security vulnerabilities. https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/352
- add alt field to rights-statement-icon in iiif manifest https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/353
- Hover text size too small on toolbar icons https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/346

## [20210730] (Sprint O21)

### Changed
- Custom container for DLG tests https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/335

### Fixed
- Fix error 500 (should be 404) when visiting URL for a non-existant collection resource (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/355)
- Update addressable (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/349)

### Staging
- Update mirador to 3.2 https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/357
- Remove mirador-share-plugin https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/358

## [20210719] (Sprint N21)

### Changed
- update addressable https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/349

### Staging
- Mirador doesn't work after hitting back button https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/340
- Mirador: Show image tools by default https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/342

## [20210625] (Sprint M21)

### Changed
- Add Banner for 7/6 outage https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/338
- Dev deploy fails to install new yarn dev dependency https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/336

### Staging
- Update manifest metadata for DLG-hosted manifests https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/325
- Download does not actually download file https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/310

## [20210614] (Sprint L21)

### Changed
- Convert secrets to encrypted credentials https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/314
- Added banner for 2 millionth digitized page https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/330

## [20210609] (Sprint K21)

### Changed
- Display 'Short Description' on Collection pages https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/322

## [20210526] (Sprint J21)

### Added
- Update sitemap during deploy process https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/300

### Staging
- Display IIIF Manifest URL on item record https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/279
- Mirador Viewer
    - Remove "elastic" view option https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/318
    - Make iframe embed option work https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/309
    - Set default Mirador layout to "no comparison" version https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/316
    - Include download link to original resolution image https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/311

## [20210507] (Sprint I21)

### Added
- Add link to explore drop down for Georgia Exhibits (for site debut on May 1) https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/302
- Add anchors to dlg home page and links to home page navbar https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/301

### Changed
- Update DLG and Me https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/306
- Update to rails 5.2.6


### Staging
- Mirador Viewer
    - Reinstall Mirador with yarn/npm (Plugins pre-req) https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/289
    - Mirador 3: downloading plug-in https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/270
    - Mirador 3: embedding/sharing plug-in https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/269
    - Mirador 3: Add image manipulation toolbar https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/271
    - Mirador 3: Please reinstate the add "add a resource" button to the IIIF toolbar https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/272
    - Mirador 3: Unable to successfully add resource to IIIF workspace (bugfix) https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/280
    - use iif_presentation flag https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/303
    - Dedicated Mirador Page https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/307
    - Mirador suggestions from GSU (https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/304):
        - IIIF drag icon
        - Don't let user close original Mirador window
        - Hide elastic mode
        - Zoom controls won't obscure part of image on initial load
        - Hide option to move side panel

## [20210326] (Sprint F21)

### Changed
- Removed link to American South on DLG public footer https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/296
- Minor improvements to sitemap generator https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/294
- Minor changes to Robots.txt https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/292


## [20210312] (Sprint E21)

### Changed
- Remove the Civil War tab on the DLG homepage https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/281

## [20210226] (Sprint D21)

### Changed
- update link label and url under Participate menu item https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/265
- New item record display for IIIF changes(Staging) https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/266
- Mirador 3: for items displaying the viewer remove the item tab display(Staging) https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/275
- Mirador 3: About this item, current Item info(Staging) https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/276
- Mirador 3: please add t.o.c. display to the image viewer(Staging) https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/273
- Remove twitter feed widget from dlg-staging(Staging) https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/282


## [20210115] (Sprint A21)

### Changed
- Update to Ruby 2.6 [254](https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/254)


## [20201218] (Sprint Y)
### Added
- Add image to footer of dlg.usg.edu [247](https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/247)

### Changed
- Verify ownership of dlg with google site console [250](https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/250)
- Change GPLA logo in the 'Our Partners & Sponsors' section of the main page [256](https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/256)
- Update DLG OHMS stylesheet
 [248](https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/248)
 - Update Blacklight to 6.23
 [255](https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/255)


## [20201204] (Sprint X)
### Added
- Add facet for full text [239](https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/239)
- Add favicons and manifest file [249](https://gitlab.galileo.usg.edu/DLG/dlg/-/issues/249)

### Changed
- None
