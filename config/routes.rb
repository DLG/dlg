# frozen_string_literal: true

Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  mount Blacklight::Engine => '/'
  mount BlacklightAdvancedSearch::Engine => '/'

  concern :searchable, Blacklight::Routes::Searchable.new
  concern :exportable, Blacklight::Routes::Exportable.new
  concern :range_searchable, BlacklightRangeLimit::Routes::RangeSearchable.new

  resources :solr_document, only: [:show], path: '/record', controller: 'records' do
    concerns :exportable
    get 'fulltext', to: 'records#fulltext'
    get 'presentation/manifest', to: 'records#presentation_manifest'
    get 'harmful_content', to: 'records#harmful_content'
  end

  resource :records, only: %i[index show] do
    get 'map', to: 'map', as: 'map'
    concerns :searchable
    concerns :range_searchable
    collection do
      get 'suggest/:facet_name', to: 'records#suggest', as: 'suggest_facet', defaults: { format: 'json' }
    end
  end

  # so related routes are available with the *_catalog_url that BL expects in addition to *_record_url
  resource :records, only:[], as: :catalog do
    concerns :searchable
  end

  get '/collection/:collection_record_id', to: 'records#index', as: 'collection_home'

  if FeatureFlags.enabled? :serials
    get '/serial/:serial_record_id', to: 'records#index', as: 'serial_home'
  else
    get '/serial/:serial_record_id', to: redirect {|path_params, _request|
      TemporaryRedirectService.legacy_route_for_serial(path_params[:serial_record_id])
    }
  end
  get '/serials/:serial_record_id', to: redirect('/serial/%{serial_record_id}')

  resource :collections, only: [:index] do
    get 'map', to: 'map', as: 'map'
    concerns :searchable
    concerns :range_searchable
    member do
      get(
        '/:collection_record_id/:collection_resource_slug',
        to: 'collection_resources#show',
        as: 'collection_resource'
      )
    end
  end

  resource :serials, only: %i[index] do
    concerns :searchable
    concerns :range_searchable
  end

  get '/collections/:collection_slug', to: 'records#index'

  resource :institutions, only: %i[index], path: '/institutions(/:first_char)', first_char: /[a-zA-Z]|0-9/, controller: 'institutions' do
    concerns :searchable
  end

  resources :institution_solr_documents, only: [:show], path: '/institutions/', controller: 'institutions' do
    concerns :exportable
  end

  resources :collection_solr_documents, only: [:show], path: '/collections/', controller: 'collections' do
    concerns :exportable
  end

  resources :serial_solr_documents, only: [:show], path: '/serials/', controller: 'serials' do
    concerns :exportable
  end

  get '/search', to: 'advanced#index', as: 'search'
  get '/counties', to: 'counties#index', as: 'counties'

  get '/iiif-viewer', to: 'iiif_viewer#iiif_viewer', as: 'iiif_viewer'
  get '/iiif-embeddable', to: 'iiif_viewer#embeddable_iiif_viewer', as: 'embeddable_iiif_viewer'

  resources :bookmarks do
    concerns :exportable
    collection do
      delete 'clear'
    end
  end

  namespace :teach do
    get 'using-materials'
  end

  namespace :participate do
    get 'contribute'
    get 'nominate'
    get 'harmful_content'
    get 'partner-services'
    post 'nomination'
  end

  namespace :about do
    get 'mission'
    get 'policy'
    get 'partners-sponsors'
    get 'api'
    get 'harmful-content'
    get 'harmful-content-form'
    post 'harmful_content_submit'
  end

  namespace :help do
    get 'search'
    get 'refine-items'
    get 'refine-collections'
    get 'map'
  end

  get '/contact(/:formtype)', to: 'contact#index', formtype: /|dlg|crdl|ghnp/, as: 'contact'

  get '/id::record_id', to: redirect('/record/%{record_id}'), status: 301

  get '/*do_tag', do_tag: /do(?:-[a-z0-9-]+)?:[a-z0-9,_-]+/, to: redirect { |path_params, _req|
    if (m=path_params[:do_tag].match /^do(?<alt>-[a-z0-9-]+)?:(?<record_id>[a-z0-9,_-]+)$/)
      "https://dlg.galileo.usg.edu/do#{m[:alt]}:#{m[:record_id]}"
    end
  }

  root to: 'homepage#index'

  match '/404', to: 'errors#file_not_found', via: :all
  match '/422', to: 'errors#unprocessable', via: :all
  match '/500', to: 'errors#internal_server_error', via: :all
end
