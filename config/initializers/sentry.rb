Sentry.init do |config|
  config.enabled_environments = %w[dev staging production]
  config.dsn = Rails.application.credentials.dig(:sentry, :dsn, Rails.env.to_sym)
  config.breadcrumbs_logger = [:active_support_logger, :http_logger]

  # Set traces_sample_rate to 1.0 to capture 100%
  # of transactions for performance monitoring.
  # We recommend adjusting this value in production.
  config.traces_sample_rate = 0.125
end
