# frozen_string_literal: true

Rails.application.config.to_prepare do
  BlacklightAdvancedSearch::QueryParser.module_eval do
    def filters
      unless @filters
        @filters = {}
        return @filters unless @params[:f_inclusive]&.respond_to?(:each_pair)
        @params[:f_inclusive].each_pair do |field, value_array|
          @filters[field] ||= value_array.dup if value_array
        end
      end
      @filters
    end
  end

  # Blacklight Advanced Search doesn't allow us to override defType on a field-by-field basis
  # This makes it honor a "force_deftype" parameter, which you can set in a search field's solr_local_parameters
  # ("force_deftype" is already the name of an option used internally in the gem)
  ParsingNesting::Tree::List.module_eval do
    def build_nested_query(embeddables, solr_params = {}, options = {})
      if solr_params[:force_deftype].present?
        options[:force_deftype] = solr_params[:force_deftype]
      end
      if solr_params[:preprocess].present? && embeddables.any?
        terms = embeddables.map(&:value).join ' '
        reference = embeddables.first
        reference.value = solr_params[:preprocess].call(terms)
        embeddables = [reference]
      end
      super(embeddables, solr_params.without(:force_deftype, :preprocess), options)
    end
  end

  BlacklightAdvancedSearch::RenderConstraintsOverride.module_eval do
    def render_constraints_query(my_params = params)
      if advanced_query.nil? || advanced_query.keyword_queries.empty?
        super(my_params)
      else
        content = []
        advanced_query.keyword_queries.each_pair do |field, query|
          label = blacklight_config.search_fields[field][:label]
          content << render_constraint_element(
            label, query,
            remove: search_action_path(remove_advanced_keyword_query(field, my_params).except(:controller, :action))
          )
        end
        if advanced_query.keyword_op == 'OR' &&
           advanced_query.keyword_queries.length > 1
          content.unshift content_tag(:span, 'Any of:', class: 'operator')
          content_tag :span, class: 'inclusive_or appliedFilter well' do
            safe_join(content.flatten, "\n")
          end
        else
          safe_join(content.flatten, "\n")
        end
      end
    end

  end
end
