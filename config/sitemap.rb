# frozen_string_literal: true

# Set the host name for URL creation
SitemapGenerator::Sitemap.default_host = 'https://dlg.usg.edu/'
SitemapGenerator::Sitemap.create_index = true

SitemapGenerator::Sitemap.create do

  def generate_sitemap_nodes (fl, fq, path, unique_id)
    cursor = '*'
    loop do
      response = Blacklight.default_index.connection.get 'select', params: {
        rows: 1000,
        fl: fl,
        fq: fq,
        cursorMark: cursor,
        sort: 'id asc'
      }
      response['response']['docs'].each do |doc|
        add "/#{path}/#{doc[unique_id]}", lastmod: doc['updated_at_dts'], priority: 0.5
      end
      break if response['nextCursorMark'] == cursor
      cursor = response['nextCursorMark']
    end
  end

  # add '/', priority: 1
  add '/teach/using-materials', priority: 0.75
  add '/participate/contribute', priority: 0.75
  add '/participate/nominate', priority: 0.75
  add '/participate/partner-services', priority: 0.75
  add '/about/mission', priority: 0.75
  add '/about/policy', priority: 0.75
  add '/about/partners-sponsors', priority: 0.75
  add '/counties', priority: 0.70
  add '/institutions', priority: 0.70
  add '/about/api', priority: 0.70

    # Add item pages
  generate_sitemap_nodes 'id, updated_at_dts', %w[display_b:1 portals_sms:georgia class_name_ss:Item], 'record', 'id'

  # Add collection pages
  generate_sitemap_nodes 'id, record_id_ss, updated_at_dts', %w[display_b:1 portals_sms:georgia class_name_ss:Collection], 'collection', 'record_id_ss'

end



